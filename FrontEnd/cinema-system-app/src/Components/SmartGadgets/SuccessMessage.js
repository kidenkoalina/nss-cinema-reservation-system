import React from "react";
import { Link, Redirect } from "react-router-dom";
import { Container,Col,Row } from "react-bootstrap";
import Loader from "./Loader";

class SuccessMessage extends React.Component{
    state = {message:null,redirect:null}
    componentDidMount(){
        if(this.props.location.message != undefined){
            const message = this.props.location.message;

            this.setState({message:message});
        }else{
            this.setState({redirect : true});
        }
        console.log(this);
    }
    render(){
        if(this.state.redirect != null){
            return <><Redirect to={"/"} /></>;
        }else{
            if(this.state.message == null){
                return (
                    <Container><Loader></Loader></Container>
                );
            }else{
                return (
                        <Container>
                            <div className="window midFull" style={{marginTop:"25px",padding:"20px"}}>
                                <h4>{this.state.message.headline}</h4>
                                <p>{this.state.message.moreInfo}</p>
                                <Link className="submit" to={"/"}>To homepage</Link>
                            </div>
                        </Container>
                );
            }
        }
    }

}
export default SuccessMessage;
