import React from "react";
import { Redirect } from "react-router-dom";
import { Container,Col,Row } from "react-bootstrap";
import Loader from "../SmartGadgets/Loader";
import {userService} from "../../AuthServices/userService"
import {authHeader} from "../../AuthServices/authHeader"
import Axios from "axios";
import ReservationTable from "../Reservation/Table";

class Reservations extends React.Component{
    state = {user:null,reservations:null,projection:null,statistics:null};
    componentDidMount(){
        const user = userService.getUser();
        this.setState({user:user});
        console.log(this);
        const { projection } = this.props.state;
        this.setState({projection:projection},()=>{
            this.fetchReservations();
            this.fetchStatistics();
        })
        console.log(this);
        
    }
    fetchStatistics(){
        const requestOptions = {
            method: 'GET',
            headers: authHeader()
        };
        let axios = Axios.create();
        axios.get("http://localhost:8080/projections/"+this.state.projection.id+"/statistics",requestOptions)
        .then((response) => {
            console.log(response);
            this.setState({statistics:response.data})
        })
        .catch((error) => {
            alert("ERROR "+error);
            console.error(error.data);
        })
    }
    fetchReservations(){
        const requestOptions = {
            method: 'GET',
            headers: authHeader()
        };
        let axios = Axios.create();
        axios.get("http://localhost:8080/projections/"+this.state.projection.id+"/reservations",requestOptions)
        .then((response) => {
            console.log(response);
            this.setState({reservations:response.data})
        })
        .catch((error) => {
            alert("ERROR "+error);
            console.error(error.data);
        })
    }
    render(){
        if(this.state.reservations == null){
            return (<Container><Loader></Loader></Container>)
        }else{
            const date = new Date(this.state.projection.date);
            const dateTime = new Date(date.getFullYear()+"-"+(date.getMonth()+1)+"-"+date.getDate()+" "+this.state.projection.time);
            let money = 0; 
            this.state.reservations.filter((reservation)=>reservation.stateOfReservation == "CONFIRMED").forEach(element => {
                money += element.projection.price*element.tickets;
                return money;
            });
            return (<Container className="window table mt-5 employeeReservation">
                    <h5 className="leftAlign ml-3" >Projection info - {this.state.projection.movie.name+" "+dateTime.getDate()+"."+(dateTime.getMonth()+1)+"."+dateTime.getFullYear()+" - "+dateTime.getHours()+":"+dateTime.getMinutes()}</h5>
                    <ul className="leftAlign ml-3" >
                        <li>Language: {this.state.projection.dabbing}</li>
                        <li>Length: {this.state.projection.movie.length} minutes</li>
                        <li>Price per ticket: {this.state.projection.price},-</li>
                        <li>{this.state.statistics}</li>
                        <li>Earned money: <strong>{money},-</strong></li>
                    </ul>
                    <h5 className="mt-2">Active reservations</h5>
                        <ReservationTable permission={true} reservations={this.state.reservations.filter((reservation)=>reservation.stateOfReservation == "CREATED")}/>
                    <h5 className="mt-2">History reservations</h5>
                        <ReservationTable permission={true} reservations={this.state.reservations.filter((reservation)=>reservation.stateOfReservation != "CREATED")}/>
                    </Container>)
        }
    }
}
export default Reservations;