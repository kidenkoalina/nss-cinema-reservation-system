import React from "react";
import { Redirect } from "react-router-dom";
import { Container,Col,Row } from "react-bootstrap";
import Loader from "../SmartGadgets/Loader";
import {userService} from "../../AuthServices/userService"
import {authHeader} from "../../AuthServices/authHeader"
import Axios from "axios";
import ReservationTable from "../Reservation/Table";
import ProjectionTable from "../Home/Projection/ProjectionTable";

class EmployeeHome extends React.Component{
    state={user:null,cinemas:null,projections:null,selectedCinema:null}
    
    componentDidMount(){
        
    }
    render(){
            return  (<Container>
                    <ProjectionTable permission={true}/>
            </Container>);
    }

}
export default EmployeeHome;