import React from "react";
import { Redirect } from "react-router-dom";
import { Container,Col,Row } from "react-bootstrap";
import Loader from "../SmartGadgets/Loader";
import {userService} from "../../AuthServices/userService"
import {authHeader} from "../../AuthServices/authHeader"
import Axios from "axios";

class ReservationForm extends React.Component{
    state = {projection:null,redirect:null,tickets:1,user:null,reservationConfirm:null,validationError:null};
    componentDidMount(){
        console.log(this);
        if(this.props.state != undefined){
            const { projection } = this.props.state;
            this.setState({projection:projection});
            console.log(projection);
        }else{
            this.setState({redirect : true});
        }
        //get logged user
        const user = userService.getUser();
        this.setState({user:user});
    }
    countPrice(e){
        const tickets = e.target.value;
        if(tickets <= 0){
            this.setState({tickets:1});
            e.target.value = 1;
        }else if(tickets > 15){
            this.setState({tickets:15});
            e.target.value = 15;
        }else{
            this.setState({tickets:tickets});
        }  
    }
    submitHandler(e){
        e.preventDefault();
        console.log(this.state.user);
        //validation
        const checkboxIsChecked = document.getElementById("gdpr").checked;
        
        if(checkboxIsChecked && this.state.tickets > 0){
            const requestOptions = {
                method: 'POST',
                headers: authHeader()
            };
            const data = {
                'person':{"id":this.state.user.id},
                'projection':{"id":this.state.projection.id},
                'tickets':this.state.tickets
            }
            const axios = Axios.create();
            return new Promise((resolve,reject) => {
                axios.post('http://localhost:8080/reservations',data,requestOptions)
                .then((response) =>{
                    if(response.status == 201){
                        alert("Your reservation have been created!");
                        this.setState({reservationConfirm:true});
                    }else{
                        alert("Error with creating reservation!");
                        throw response;
                    }
                })
                .catch((error)=>{
                    this.setState({validationError:error.response.data});
                })
            })
        }else{
            this.setState({validationError:"You must agree with GDPR conditions"});
        }
    }
    render(){
        
        if(this.state.redirect != null){
            return <><Redirect to={"/"} /></>;
        }else{
            if(this.state.projection == null){
                return (
                    <Container><Loader></Loader></Container>
                );
            }else{
                if(this.state.reservationConfirm){
                    return (
                            <Redirect to={{
                                pathname: '/reservation/success',
                                message: {
                                    headline: "Reservation was successfully created!",
                                    moreInfo:"Now you can pick up your tickets at cinema"
                                }
                            }}/>
                            );
                }
                const projectionDate = new Date(this.state.projection.date);
                let projectionTime = this.state.projection.time;
                projectionTime = projectionTime.substring(0,5);
                return ( 
                    <Container className="reservationForm">
                        <form>
                            <div className="window midFull">      
                                <h4>Create new reservation</h4>
                                <Row>
                                    <Col>
                                        <label>Movie</label>
                                        <h6>{this.state.projection.movie.name}</h6>
                                    </Col>
                                    <Col>
                                        <label>Dabbing</label>
                                        <h6>{this.state.projection.dabbing}</h6>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col>
                                        <label>Date</label>
                                        <h6>{projectionDate.getUTCDate()}.{projectionDate.getUTCMonth()+1}.{projectionDate.getUTCFullYear()}</h6>
                                    </Col>
                                    <Col>
                                        <label>Start time</label>
                                        <h6>{projectionTime}</h6>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col>
                                        <label>Cinema</label>
                                        <h6>{this.state.projection.cinemahall.cinema.name}</h6>
                                    </Col>
                                    <Col>
                                        <label>Town</label>
                                        <h6>{this.state.projection.cinemahall.cinema.town}</h6>
                                    </Col>
                                </Row>
                            </div>  
                            <div className="window midFull">
                                <h4>Confirm your offer</h4>
                                <Row>
                                    <Col>
                                        <label>Number of tickets</label>
                                    </Col>
                                    <Col>
                                        <input type="number" min="1" max="15" defaultValue="1" onChange={(e) => this.countPrice(e)}/>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col>
                                        <label>Price per ticket</label>
                                    </Col>
                                    <Col>
                                        <h6>{this.state.projection.price},-</h6>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col>
                                        <label>Summary price</label>
                                    </Col>
                                    <Col>
                                        <h6>{(this.state.tickets*this.state.projection.price)},-</h6>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col>
                                        <label className="containerInput">I agree with GDPR conditions
                                            <input id="gdpr" type="checkbox"/>
                                            <span className="checkmark"></span>
                                        </label>
                                        <span className="validError">{this.state.validationError}</span>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col>
                                        <button type="submit" className="submit" onClick={(e)=>this.submitHandler(e)}>Confirm</button>
                                    </Col>
                                </Row>
                            </div>
                        </form>  
                    </Container>
                );
            }
        }
    }
}
export default ReservationForm;