import React from "react";
import {authHeader} from "../../AuthServices/authHeader"
import Axios from "axios";

class Reservation extends React.Component{
    state={reservation:null,permission:null}
    componentDidMount(){
        const reservation = this.props.reservation;
        this.setState({reservation:reservation});
        console.log(reservation);
        const permission = this.props.permission;
        this.setState({permission:permission});
    }
    popupCancel(e){
        e.preventDefault();
        if(window.confirm("Are you sure?")){
            this.cancelReservation();
        }
    }
    cancelReservation(){
        const axios = Axios.create();
        const requestOptions = {
            headers: authHeader()
        };
        axios.delete("http://localhost:8080/reservations/"+this.state.reservation.id+"/cancel",requestOptions)
        .then((response) => {
            console.log(response);
            if(response.status == 204){
                window.alert("Reservation was cancelled");
                window.location.reload();
            }
        })
        .catch((error) => {
            console.log(error);
            console.error(error);
        });
    }
    popupConfirm(e){
        e.preventDefault();
        if(window.confirm("Are you sure?")){
            this.confirmReservation();
        }
    }
    confirmReservation(){
        const axios = Axios.create();
        const requestOptions = {
            method : "GET",
            headers: authHeader()
        };
        console.log("EEEFE");
        axios.get("http://localhost:8080/reservations/"+this.state.reservation.id+"/confirm",requestOptions)
        .then((response) => {
            console.log("SDFSDFDSFSDFSDF");
            console.log(response);
            if(response.status == 204){
                window.alert("Reservation was confirmed");
                window.location.reload();
            }
        })
        .catch((error) => {
            console.log(error);
            console.error(error);
        });
    }
    render(){

        if(this.state.reservation==null){
            return <tr></tr>;
        }else{
            const date = new Date(this.state.reservation.projection.date);
            const dateTime = new Date(date.getFullYear()+"-"+(date.getMonth()+1)+"-"+date.getDate()+" "+this.state.reservation.projection.time);
            const  state = this.state.reservation.stateOfReservation;
            let action = null;
            if(state == "CREATED"){
                action = <a href="/cancel" className="cancelReservation" onClick={(e)=>this.popupCancel(e)}>Cancel</a>;
                if(this.state.permission){
                    action = <><a href="/cancel" className="cancelReservation" onClick={(e)=>this.popupCancel(e)}>Cancel</a><a href="/confirm" className="confirmReservation" onClick={(e)=>this.popupConfirm(e)}>Confirm</a></>
                }
            }else if(state == "CANCELLED") {
                action = <span>Canceled</span>
            }else{
                action = <span>Confirmed</span>
            }
            return <tr>
                    {(this.state.permission? 
                    <>
                    <td>{this.state.reservation.person.name}</td>
                    <td>{this.state.reservation.person.surname}</td>
                    <td>{this.state.reservation.person.email}</td>
                    <td>{this.state.reservation.tickets}</td>
                    <td>{this.state.reservation.projection.price*this.state.reservation.tickets},-</td>
                    <td>{this.state.reservation.stateOfReservation}</td>
                    <td>{action}</td>
                    </>
                    :
                    <>
                    <td>{this.state.reservation.projection.cinemahall.cinema.name}</td>
                    <td>{this.state.reservation.projection.cinemahall.number}</td>
                    <td>{this.state.reservation.projection.movie.name}</td>
                    <td>{dateTime.getDate()+"."+(dateTime.getMonth()+1)+"."+dateTime.getFullYear()+" - "+dateTime.getHours()+":"+dateTime.getMinutes()}</td>
                    <td>{this.state.reservation.projection.price},-</td>
                    <td>{this.state.reservation.tickets}</td>
                    <td>{action}</td>
                    </>
                    )}
                </tr>
        }
    }
}
export default Reservation;