import React from "react";
import { Container,Col,Row } from "react-bootstrap";
import Loader from "../SmartGadgets/Loader";
import Reservation from "./Get";
import { Link } from "react-router-dom";

class ReservationTable extends React.Component{
    state={reservations:null,type:null,permission:null}
    componentDidMount(){
        const reservations = this.props.reservations;
        const type = this.props.type;
        const permission = this.props.permission;
        this.setState({permission:permission});
        this.setState({reservations:reservations});
        this.setState({type:type});
        console.log(this);
    }
    render(){
        if(this.state.reservations == null){
            return <Container><Loader></Loader></Container>;
        }else if(this.state.reservations.length > 0){
            return <>
                    <table className="center reservation">
                        <thead>
                            <tr>
                                {(this.state.permission) ? <><th>First name</th><th>Last name</th><th>E-mail</th><th>Tickets</th><th>Summarry price</th><th>Status</th><th>Action</th></>:
                                <>
                                <th>Cinema</th>
                                <th>Hall</th>
                                <th>Movie</th>
                                <th>Date and Time</th>
                                <th>Price</th>
                                <th>Tickets</th>
                                <th>Action</th>
                                </>
                                }
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.reservations.map((reservation) => {
                                return <Reservation permission={this.state.permission} key={reservation.id} reservation={reservation} />
                            })}
                        </tbody>
                    </table>
            </>
        }else{
            if(this.state.type == "active"){
                return <>
                    <p>You have any {this.state.type} reservations...</p>
                </>;
            }else{
                return <>
                    <p>No history</p>
                </>;
            }
            
        }
    }
}
export default ReservationTable;