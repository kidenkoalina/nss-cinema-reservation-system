import React from "react";
import { Redirect, Link } from "react-router-dom";
import { Container,Col,Row, Button } from "react-bootstrap";
import Loader from "../SmartGadgets/Loader";
import {userService} from "../../AuthServices/userService"
import rules from "../../Files/validationRules.json";
import Form from "react-bootstrap/Form";
import {authHeader} from "../../AuthServices/authHeader"
import Axios from "axios";

import {formValidation,validationFeedback,validationClassName} from "../../../src/Validator";

class UserSettings extends React.Component{
    state = {
        form: {
            isValid: false,
            elements: {
                name: {
                    touched: false,
                    valid: false,
                    validationRules: rules.editUser.firstName
                },
                surname: {
                    touched: false,
                    valid: false,
                    validationRules: rules.editUser.lastName
                },
                email: {
                    touched: false,
                    valid: false,
                    validationRules: rules.editUser.email
                },
                password: {
                    touched: false,
                    valid: false,
                    validationRules: rules.editUser.password
                }
            },
        },
        user: null,
        newUser:null,
        editSuccess:null
    };
    componentDidMount(){
        const user = userService.getUser();
        this.setState({user:user});
        this.setState({newUser:user});
    }
    inputUpdateHandler = async (event, nameOfFormInput, select) => {
        let newStateUser = { ...this.state.newUser };
        console.log(this.state.newUser);
        console.log(Object.keys(newStateUser));
         if (Object.keys(newStateUser).includes(nameOfFormInput)) {
            if (select && event.target.value == 0) {
                newStateUser[nameOfFormInput] = null;
            } else {
                newStateUser[nameOfFormInput] = event.target.value;
            }
            await this.setState({ newUser: newStateUser });
        }

        if (this.state.form.elements[nameOfFormInput].touched) {
            this.validateForm();
        }
        console.log(this.state.newUser);
    };

    validateForm = async () => {
        let newState = { ...this.state.form };
        formValidation(newState, this.state.newUser);
        await this.setState({ form: newState });
    };

    submitHandler = async (event) => {
        event.preventDefault();
        console.log(this.state.newUser);
        this.validateForm();
        //kontrola hesla
        const newPassword = document.querySelector("input[type='password']").value;

        if(newPassword != ""){
            console.log(newPassword);
            this.state.newUser.password = newPassword;
            console.log(this.state.newUser);
        }
        console.log(this.state.form.isValid);
        console.log(this.state.newUser);
            const requestOptions = {
                method: 'POST',
                headers: authHeader()
            };
            /*const axios = Axios.create();
            axios.put('http://localhost:8080/user/'+this.state.user.id,this.state.newUser,requestOptions)
            .then((response) =>{
                console.log(response);
                
            })
            .catch((error) => {
                console.log(error);
                console.error(error);
            });*/
        
        
    };
    render(){
        if(this.state.user == null){
            return (<Container><Loader></Loader></Container>);
        }
        return (
            <Container className="register_container">
                <Row>
                    <Col>
                        <div className="window midFull">
                            <h5>Profile settings</h5>
                            <Form
                                className="registerUserForm mt-3 mb-3"
                                onSubmit={this.submitHandler}>
                                <Form.Row>
                                    <Form.Group as={Col}>
                                        <Form.Label>First name</Form.Label>
                                        <Form.Control
                                        defaultValue={this.state.newUser.name}
                                            maxLength="30"
                                            autoComplete="name"
                                            placeholder="Enter your first name"
                                            onChange={(event) =>
                                                this.inputUpdateHandler(
                                                    event,
                                                    "name"
                                                )
                                            }
                                            className={validationClassName(
                                                "name",
                                                this.state.form
                                            )}
                                        />
                                        <div className="invalid-feedback">
                                            {validationFeedback(
                                                "name",
                                                this.state.form
                                            )}
                                        </div>
                                    </Form.Group>
                                    <Form.Group as={Col}>
                                        <Form.Label>Last name</Form.Label>
                                        <Form.Control
                                            defaultValue={this.state.newUser.surname}
                                            maxLength="30"
                                            placeholder="Enter your last name"
                                            onChange={(event) =>
                                                this.inputUpdateHandler(
                                                    event,
                                                    "surname"
                                                )
                                            }
                                            className={validationClassName(
                                                "surname",
                                                this.state.form
                                            )}
                                        />
                                        <div className="invalid-feedback">
                                            {validationFeedback(
                                                "surname",
                                                this.state.form
                                            )}
                                        </div>
                                    </Form.Group>
                                </Form.Row>
                                <Form.Row>
                                    <Form.Group as={Col}>
                                        <Form.Label>E-mail</Form.Label>
                                        <Form.Control
                                            defaultValue={this.state.newUser.email}
                                            maxLength="50"
                                            name="email"
                                            autoComplete="email"
                                            type="email"
                                            placeholder="Enter your valid e-mail"
                                            onChange={(event) =>
                                                this.inputUpdateHandler(
                                                    event,
                                                    "email"
                                                )
                                            }
                                            className={validationClassName(
                                                "email",
                                                this.state.form
                                            )}
                                        />
                                        <div className="invalid-feedback">
                                            {validationFeedback(
                                                "email",
                                                this.state.form
                                            )}
                                        </div>
                                    </Form.Group>
                                </Form.Row>
                                <Form.Row>
                                    <Form.Group as={Col}>
                                        <Form.Label>Password</Form.Label>
                                        <Form.Control
                                            type="password"
                                            placeholder="Password"
                                            onChange={(event) =>
                                                this.inputUpdateHandler(
                                                    event,
                                                    "password"
                                                )
                                            }
                                            className={validationClassName(
                                                "password",
                                                this.state.form
                                            )}
                                        />
                                        <div className="invalid-feedback">
                                            {validationFeedback(
                                                "password",
                                                this.state.form
                                            )}
                                        </div>
                                    </Form.Group>
                                </Form.Row>
                                <Form.Row>
                                    <Button
                                        className="submit"
                                        variant="primary"
                                        type="submit"
                                    >
                                        Save changes
                                    </Button>
                                </Form.Row>
                            </Form>
                        </div>
                    </Col>
                </Row>
            </Container>
        );
    }
}
export default UserSettings;