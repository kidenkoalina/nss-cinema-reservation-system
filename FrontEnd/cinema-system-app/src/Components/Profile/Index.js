import React from "react";
import { Redirect, Link } from "react-router-dom";
import { Container,Col,Row } from "react-bootstrap";
import Loader from "../SmartGadgets/Loader";
import {userService} from "../../AuthServices/userService"

import userIcon from "../../Files/images/user.png"
import {authHeader} from "../../AuthServices/authHeader"
import Axios from "axios";
import ReservationTable from "../Reservation/Table";

class UserProfile extends React.Component{
    state = {user:null,reservations:null};
    async componentDidMount(){
        const user = userService.getUser();
        this.setState({user:user},()=>{  
            this.fetchUserReservation();
        });
    }
    fetchUserReservation(){
        const requestOptions = {
            headers: authHeader()
        };
        let axios = Axios.create(); 
        axios.get("http://localhost:8080/user/"+this.state.user.id+"/reservations",requestOptions)
        .then((response) =>{
            console.log(response);
            if(response.status == 200){
                this.setState({reservations:response.data});
            }
        })
        .catch((error) => {
            console.log(error);
            console.error(error);
        });
    }
    render(){
        if(this.state.user == null || this.state.reservations == null){
            return <Container><Loader></Loader></Container>
        }else{
            return (
                <Container className="userProfile mt-5">
                    
                    <Row> 
                        <Col xs={4}>
                            <div className="window userInfo">
                                <h5 className="mt-3">User profile</h5>
                                <div className="userIcon">
                                    <img src={userIcon} />
                                </div>
                                <ul>
                                    <li>
                                        <span>{this.state.user.name} {this.state.user.surname}</span>
                                    </li>

                                    <li>
                                        <strong>E-mail: </strong>
                                        <span>{this.state.user.email}</span>
                                    </li>
                                    <li>
                                        <strong>Role: </strong>
                                        <span>{this.state.user.role}</span>
                                    </li>
                                    <li>
                                        <Link to="/user/settings">Profile settings</Link>
                                    </li>
                                </ul>
                            </div>    
                        </Col>
                        <Col xs={8}>
                            <div className="window table">
                                {/* Active*/}
                                <strong>New reservations</strong>
                                <ReservationTable type={"active"} reservations={this.state.reservations.filter((reservation)=>reservation.stateOfReservation == "CREATED")}/>
                                <strong>Reservations history</strong>
                                {/* Old or cancelled */}  
                                <ReservationTable type={"expired"} reservations={this.state.reservations.filter((reservation)=>reservation.stateOfReservation != "CREATED")}/>
                            </div>    
                        </Col>
                    </Row>      
                </Container>
            );
        }
        
    }

}
export default UserProfile;