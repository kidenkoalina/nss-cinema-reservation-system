import React from "react";
import { Container, Spinner } from "react-bootstrap";
import { Redirect } from "react-router-dom";

import {userService} from "../../AuthServices/userService"

class Logout extends React.Component {
    
    render() {
        if (userService.isLoggedIn()) {
           userService.logout()
           .then(() => {
                window.location.reload(true);
           })
            
        } else {
            return (
                <Container className="p-5 mt-5">
                    <Spinner animation="border" role="status">
                        <span className="sr-only">Loading...</span>
                    </Spinner>
                </Container>
            );
        }
    }
}

export default Logout;
