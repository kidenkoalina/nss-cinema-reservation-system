import React from "react";
import Form from "react-bootstrap/Form";
import { Container, Col, Button, Row, Spinner } from "react-bootstrap";
import rules from "../../Files/validationRules.json";
import { formValidation,validationFeedback,validationClassName} from "../../Validator";
import MyAlert from "../SmartGadgets/MyAlert";
import { withRouter } from "react-router-dom";
import {userService} from "../../AuthServices/userService"
import Axios from "axios";
class Login extends React.Component {
    state = {
        form: {
            isValid: false,
            elements: {
                email: {
                    touched: false,
                    valid: false,
                    validationRules: rules.login.email,
                },
                password: {
                    touched: false,
                    valid: false,
                    validationRules: rules.login.password,
                },
            },
        },
        user: {
            email: null,
            password: null,
        }
    };

    inputUpdateHandler = async (event, nameOfFormInput) => {
        const stringProperties = ["email", "password"];
        let newState = { ...this.state.user };
        //string inputs
        if (stringProperties.includes(nameOfFormInput)) {
            newState[nameOfFormInput] = event.target.value;
        }
        await this.setState({ user: newState });
        if (
            this.state.form.elements.hasOwnProperty(nameOfFormInput) &&
            this.state.form.elements[nameOfFormInput].touched
        ) {
            this.validateForm();
        }
        console.log(this.state.user);
    };

    submitHandler = async (event) => {
        const axios = Axios.create();
        event.preventDefault();
        await this.validateForm();
        if (this.state.form.isValid) {
            userService.login(this.state.user.email,this.state.user.password)
            .then((user) =>{
                const authdata = window.btoa(this.state.user.email + ':' + this.state.user.password);
                userService.loadUser(authdata)
                .then((user) => {
                    localStorage.setItem('user', JSON.stringify(user));
                    window.location.reload(true);
                });
            })
            .catch((error)=>{
                console.error(error);
                console.log(this.state);
                //please ignore this block :--)
                document.querySelector("input[type=email]").classList.remove("is-valid");
                document.querySelector("input[type=password]").classList.remove("is-valid");
                document.querySelector("input[type=email]").classList.add("invalid");
                document.querySelector("input[type=password]").classList.add("invalid");
                //
                
                this.props.history.push({
                    message: (
                         <MyAlert
                         variant="danger"
                         text="Email or password is invalid."
                         heading="Login not successful!"/>
                    )
                });
            });
        }
    };
    validateForm = async () => {
        console.log("in validation");
        const newState = { ...this.state.form };
        formValidation(newState, this.state.user);
        await this.setState({ form: newState });
    };
    render() {
        let alert = null;
        if (
            this.props.location &&
            this.props.location.hasOwnProperty("message")
        ) {
            alert = this.props.location.message;
        }
        return (
            <Container className="login_container">
                <Row>
                    <Form
                        className="window radius login_form"
                        onSubmit={this.submitHandler}
                    >
                        <Form.Group controlId="formBasicEmail">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control
                                type="email"
                                placeholder="Enter email"
                                onChange={(event) =>
                                    this.inputUpdateHandler(event, "email")
                                }
                                className={validationClassName(
                                    "email",
                                    this.state.form
                                )}
                            />
                            <div className="invalid-feedback">
                                {validationFeedback("email", this.state.form)}
                            </div>
                        </Form.Group>
                        <Form.Group controlId="formBasicPassword">
                            <Form.Label>Password</Form.Label>
                            <Form.Control
                                type="password"
                                placeholder="Password"
                                onChange={(event) =>
                                    this.inputUpdateHandler(event, "password")
                                }
                                className={validationClassName(
                                    "password",
                                    this.state.form
                                )}
                            />
                            <div className="invalid-feedback">
                                {validationFeedback(
                                    "password",
                                    this.state.form
                                )}
                            </div>
                        </Form.Group>
                        <Button
                            variant="primary"
                            type="submit"
                            className="submit"
                        >
                            Login
                        </Button>
                    </Form>
                </Row>
                {alert}
            </Container>
        );
    }
}

export default withRouter(Login);
