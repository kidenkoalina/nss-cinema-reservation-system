import React from "react";
import Form from "react-bootstrap/Form";
import { Col, Button, Row, Spinner } from "react-bootstrap";
import { Container } from "react-bootstrap";
import rules from "../Files/validationRules.json";
import {formValidation,validationFeedback,validationClassName} from "../Validator";
import { withRouter,Redirect } from "react-router-dom";
import Axios from "axios";

class Register extends React.Component {
    state = {
        form: {
            isValid: false,
            elements: {
                firstName: {
                    touched: false,
                    valid: false,
                    validationRules: rules.registration.firstName,
                },
                lastName: {
                    touched: false,
                    valid: false,
                    validationRules: rules.registration.lastName,
                },
                email: {
                    touched: false,
                    valid: false,
                    validationRules: rules.registration.email,
                },
                password: {
                    touched: false,
                    valid: false,
                    validationRules: rules.registration.password,
                },
                password_control: {
                    touched: false,
                    valid: false,
                    validationRules: rules.registration.password_control,
                }
            },
        },
        user: {
            firstName: null,
            lastName: null,
            email: null,
            password: null,
            password_control: null,
        },
        registerSucces:null
    };
    inputUpdateHandler = async (event, nameOfFormInput, select) => {
        let newStateUser = { ...this.state.user };
         if (Object.keys(newStateUser).includes(nameOfFormInput)) {
            if (select && event.target.value == 0) {
                newStateUser[nameOfFormInput] = null;
            } else {
                newStateUser[nameOfFormInput] = event.target.value;
            }
            await this.setState({ user: newStateUser });
        } else if (nameOfFormInput == "password_control") {
            await this.setState({ password_control: event.target.value });
        }

        if (this.state.form.elements[nameOfFormInput].touched) {
            this.validateForm();
        }
        console.log(this.state);
    };

    validateForm = async () => {
        let newState = { ...this.state.form };
        formValidation(newState, this.state.user);
        await this.setState({ form: newState });
    };

    submitHandler = async (event) => {
        event.preventDefault();
        console.log(this.state.user);
        this.validateForm();
        let user = {
            name: this.state.user.firstName,
            surname: this.state.user.lastName,
            email: this.state.user.email,
            password: this.state.user.password,
            role: "PERSON",
            person: true
        };
        
        const axios = Axios.create();
        console.log(user);
        axios.post('http://localhost:8080/user',user)
        .then((response) =>{
            console.log(response);
            if(response.status == 201){
                this.setState({registerSucces:true});
            }
        })
        .catch((error) => {
            console.log(error);
            console.error(error);
        });
    };
    render() {
        if(this.state.registerSucces){
            return (
                    <Redirect to={{
                        pathname: '/register/success',
                        message: {
                            headline: "Registration was successful!",
                            moreInfo:"Now you can login"
                        }
                    }}/>
                    );
        }else{
            return (
                <Container className="register_container">
                    <Row>
                        <Col>
                            <div className="window midFull">
                                <h5>Register new user</h5>
                                <Form
                                    className="registerUserForm mt-3 mb-3"
                                    onSubmit={this.submitHandler}>
                                    <Form.Row>
                                        <Form.Group as={Col}>
                                            <Form.Label>First name</Form.Label>
                                            <Form.Control
                                                maxLength="30"
                                                autoComplete="name"
                                                placeholder="Enter your first name"
                                                onChange={(event) =>
                                                    this.inputUpdateHandler(
                                                        event,
                                                        "firstName"
                                                    )
                                                }
                                                className={validationClassName(
                                                    "firstName",
                                                    this.state.form
                                                )}
                                            />
                                            <div className="invalid-feedback">
                                                {validationFeedback(
                                                    "firstName",
                                                    this.state.form
                                                )}
                                            </div>
                                        </Form.Group>
                                        <Form.Group as={Col}>
                                            <Form.Label>Last name</Form.Label>
                                            <Form.Control
                                                maxLength="30"
                                                placeholder="Enter your last name"
                                                onChange={(event) =>
                                                    this.inputUpdateHandler(
                                                        event,
                                                        "lastName"
                                                    )
                                                }
                                                className={validationClassName(
                                                    "lastName",
                                                    this.state.form
                                                )}
                                            />
                                            <div className="invalid-feedback">
                                                {validationFeedback(
                                                    "lastName",
                                                    this.state.form
                                                )}
                                            </div>
                                        </Form.Group>
                                    </Form.Row>
                                    <Form.Row>
                                        <Form.Group as={Col}>
                                            <Form.Label>E-mail</Form.Label>
                                            <Form.Control
                                                maxLength="50"
                                                name="email"
                                                autoComplete="email"
                                                type="email"
                                                placeholder="Enter your valid e-mail"
                                                onChange={(event) =>
                                                    this.inputUpdateHandler(
                                                        event,
                                                        "email"
                                                    )
                                                }
                                                className={validationClassName(
                                                    "email",
                                                    this.state.form
                                                )}
                                            />
                                            <div className="invalid-feedback">
                                                {validationFeedback(
                                                    "email",
                                                    this.state.form
                                                )}
                                            </div>
                                        </Form.Group>
                                    </Form.Row>
                                    <Form.Row>
                                        <Form.Group as={Col}>
                                            <Form.Label>Password</Form.Label>
                                            <Form.Control
                                                type="password"
                                                placeholder="Password"
                                                onChange={(event) =>
                                                    this.inputUpdateHandler(
                                                        event,
                                                        "password"
                                                    )
                                                }
                                                className={validationClassName(
                                                    "password",
                                                    this.state.form
                                                )}
                                            />
                                            <div className="invalid-feedback">
                                                {validationFeedback(
                                                    "password",
                                                    this.state.form
                                                )}
                                            </div>
                                        </Form.Group>
                                    </Form.Row>
                                    <Form.Row>
                                  
                                        <Form.Group as={Col}>
                                            <Form.Label>Password again</Form.Label>
                                            <Form.Control
                                                type="password"
                                                placeholder="Password again"
                                                onChange={(event) =>
                                                    this.inputUpdateHandler(
                                                        event,
                                                        "password_control"
                                                    )
                                                }
                                                className={validationClassName(
                                                    "password_control",
                                                    this.state.form
                                                )}
                                            />
                                            <div className="invalid-feedback">
                                                {validationFeedback(
                                                    "password_control",
                                                    this.state.form
                                                )}
                                            </div>
                                        </Form.Group>
                                    </Form.Row>
                                    <Form.Row>
                                        <Button
                                            className="submit"
                                            variant="primary"
                                            type="submit"
                                        >
                                            Create new account
                                        </Button>
                                    </Form.Row>
                                </Form>
                            </div>
                        </Col>
                    </Row>
                </Container>
            );
        }
        
    }
}

export default withRouter(Register);
