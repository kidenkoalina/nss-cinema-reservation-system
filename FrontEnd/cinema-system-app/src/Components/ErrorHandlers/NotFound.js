import React from "react";
import icon from "../../Files/images/404.png"
class NotFound extends React.Component{
    
    render(){
        return <div className="window midFull mt-5 notFound" >
            <img src={icon} />
            <p>404 nebo Not Found je stavový kód ze skupiny klientských chyb, vracený serverem v případě, že požadovaný soubor nebyl nalezen.</p>
            <p><strong>Vytáhni v hospodě:</strong> Stavový kód HTTP protokolu navrhl Timothy Bernes-Lee.</p>
        </div>;
    }
    
}
export default NotFound;