import React from "react";
import Form from "react-bootstrap/Form";
import { Col, Button, Row, Spinner } from "react-bootstrap";
import { Container } from "react-bootstrap";
import SessionGroup from "./SessionGroup";
import ButtonInRow from "../../SmartGadgets/ButtonInRow";
import rules from "../../../Files/validationRules.json";
import { withRouter } from "react-router-dom";
import {
    formValidation,
    validationFeedback,
    validationClassName,
} from "../../../Validator";
import DatePicker from "react-datepicker";
import { authHeader } from "../../../AuthServices/authHeader";
import Axios from "axios";
import Loader from "../../SmartGadgets/Loader"

class ProjectionForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            cinemaHalls: null,
            cinemas: null,
            movies: null,
            selectedCinema: null,
            selectedCinemaHalls: null,
            form: {
                isValid: false,
                elements: {
                    price: {
                        touched: false,
                        valid: false,
                        validationRules: rules.projection.price,
                    }
                },
            },
            projection: {
                movie: null,
                dabing: null,
                date: new Date(),
                cinemaHall: null,
                price: null
            },
        };
    }
    //fetch all data
    async componentDidMount() {
        const requestSettings = {
            method: "GET",
            credentials: "include",
            headers: {
                "Content-Type": "application/json",
            },
        };
        //fetch movies
        await fetch(`http://localhost:8080/movies`, requestSettings)
            .then((response) => {
                if (response.ok) return response.json();
                else console.error(response.status);
            })
            .then((data) => {
                this.setState({ movies: data });
            })
            .catch((error) => {
                console.error(error);
            });
        //fetch cinemas
        await fetch(`http://localhost:8080/cinemas`, requestSettings)
            .then((response) => {
                if (response.ok) return response.json();
                else console.error(response.status);
            })
            .then((data) => {
                console.log(data);
                this.setState({ cinemas: data });
            })
            .catch((error) => {
                console.error(error);
            });
    }
    //set functions
    setDate = date => {
        const projection = this.state.projection;
        projection.date = date;
        this.setState({
            projection: projection
        });
        
    };
    setMovie = movie => {
        console.log(movie);
        const projection = this.state.projection;
        projection.movie = movie;
        this.setState({
            projection: projection
        });
    };
    setDabing = dabing => {
        const projection = this.state.projection;
        projection.dabing = dabing;
        this.setState({
            projection: projection
        });
    };
    setHall = hall => {
        console.log(hall);
        const projection = this.state.projection;
        projection.cinemaHall = hall;
        this.setState({
            projection: projection
        });
    };
    //on change input handler
    inputUpdateHandler = async (event, nameOfInput) => {
        const newState = { ...this.state };
        newState[nameOfInput] = event.target.value;
        const projection = this.state.projection;
        projection[nameOfInput] = event.target.value
        await this.setState({
            projection: projection
        });
        console.log(this.state);
    };
    /**
     * Submit button
     */
    submitHandler = async (event) => {
        event.preventDefault();

        console.log("submit");
        console.log(this.state.projection);
        if(this.validateForm()){
            console.log("VALIDNI");
            this.addProjection();
        }else{
            const error = document.querySelector(".validError");
            console.log(error);
            error.classList.add("visible");
        }        
        /*if (this.state.form.isValid) {
            
        }*/
        
    };

    addProjection = () => {
        const axios = Axios.create();
        const requestOptions = {
            method: 'POST',
            headers: authHeader()
        };
        const date = new Date(this.state.projection.date);
        console.log(date.getFullYear()+"-0"+(date.getUTCMonth()+1)+"-"+date.getDate());
       const body = {
            "dabing": this.state.projection.dabing,
            "date": date.getFullYear()+"-"+(date.getMonth()+1)+"-"+date.getDate(),
            "price": this.state.projection.price,
            "time": date.getHours()+":"+date.getMinutes(),
            "cinemahall": {"id" : this.state.projection.cinemaHall},
            "movie": {"id" : this.state.projection.movie}
        }
        const body2 =  {
            "dabing": this.state.projection.dabing,
            "date": date.getFullYear()+"-0"+(date.getMonth()+1)+"-"+date.getDate(),
            "price": this.state.projection.price,
            "time": date.getHours()+":"+date.getMinutes(),
            "cinemahall": {"id" : this.state.projection.cinemaHall},
            "movie": {"id" : this.state.projection.movie}
        }
        console.log(body);
        axios.post("http://localhost:8080/projections",body2,requestOptions)
        .then((response) => {
            if(response.status == 201){
                alert("Projection was created!");
                window.location.href = "/projections";
            }else{
                alert("Error - Projection was not created!");
            }
        })
        /*fetch("http://localhost:8080/projections", {
            method: "POST",
            mode: "cors",
            credentials: "include",
            headers: authHeader(),
                body: JSON.stringify(this.state.projection),
            }).then((response) => {
                if (response.ok) this.props.history.push("/projections");
                else console.log("Error: something went wrong");
            });*/
    }

    setCinema = (event) => {
        console.log(event.target.value);
        this.setState({
            selectedCinema: event.target.value
        }, () => {
            const requestOptions = {
                method: 'GET',
                headers: authHeader()
            };
            //fetch all halls from cinema
            const axios = Axios.create();
            axios.get("http://localhost:8080/cinemas/" + this.state.selectedCinema + "/halls", requestOptions)
            .then((response) => {
                console.log(response.data);
                this.setState({ selectedCinemaHalls: response.data })
            });
        });
    };

    validateForm = () => {
        console.log("in validation");
        const newState = { ...this.state.form };
        let val = true;
        for(var k in this.state.projection) {
            if(this.state.projection[k] == null){
                console.log("REUT");
                val = false;
            }
        }
        console.log(val);
        return val;
    };

    render() {
        if (this.state.movies == null || this.state.cinemas == null) {
            return (
                <Container><Loader></Loader></Container>
            );
        } else {
            let movieOptions = null;
            let dabingOptions = null;
            let cinemaOptions = null;
            let cinemaHallOptions = null;
            let dabingArray = [];
            dabingArray.push(<option defaultValue selected disabled>Select dabing..</option>);
            dabingArray.push(<option key={1}>CZ</option>);
            dabingArray.push(<option key={2}>ENG</option>);
            dabingArray.push(<option key={3}>SK</option>);
            dabingArray.push(<option key={4}>RUS</option>);
            dabingOptions = (
                <>
                    <Form.Control
                        as="select"
                        onChange={(event) =>
                            this.setDabing(event.target.value)
                        }
                    >
                        {dabingArray}
                    </Form.Control>
                </>
            )
            if (this.state.movies != null) {
                movieOptions = (
                    <>
                        <Form.Control
                            as="select"
                            onChange={(event) =>
                                this.setMovie(event.target.value)
                            }
                        >   <option defaultValue>Select movie...</option>
                            {this.state.movies.map((movie) => {
                                return <option key={movie.id} value={movie.id}>{movie.name}</option>
                            })}
                        </Form.Control>
                    </>
                );
            }
            if (this.state.cinemas != null) {
                cinemaOptions = (
                    <>
                        <Form.Control
                            as="select"
                            onChange={(event) =>
                                this.setCinema(event)
                            }
                        >
                            <option selected defaultValue disabled>Select cinema...</option>
                            {this.state.cinemas.map((cinema) => {
                                return <option key={cinema.id} value={cinema.id}>{cinema.name}</option>
                            })}
                        </Form.Control>
                    </>
                );
            }
            if (this.state.selectedCinemaHalls != null) {
                cinemaHallOptions = (
                    <>
                        <Form.Control
                            as="select"
                            onChange={(event) =>
                                this.setHall(event.target.value)
                            }
                        >
                            <option selected defaultValue disabled>Select hall...</option>
                            {this.state.selectedCinemaHalls.map((hall) => {
                                return <option key={hall.id} value={hall.id}>{hall.number}</option>
                            })}
                        </Form.Control>
                    </>
                );
            }
            return (
                <Container className="window midFull mt-5 createProjection">
                    <Form className="mt-3 mb-5" onSubmit={this.submitHandler}>
                        <h5>Create Projection</h5>
                        <Form.Row>
                            <Form.Group as={Col} controlId="formMovie">
                                <Form.Label>Movie</Form.Label>
                                {movieOptions}
                            </Form.Group>
                            <Form.Group as={Col} controlId="formDabing">
                                <Form.Label>Dabing</Form.Label>
                                {dabingOptions}
                            </Form.Group>
                        </Form.Row>
                        <Form.Row>
                            <Form.Group as={Col} controlId="formGridprice">
                                <Form.Label>Price</Form.Label>
                                <Form.Control
                                    type="number"
                                    placeholder="Enter price."
                                    onChange={(event) =>
                                        this.inputUpdateHandler(event, "price")
                                    }
                                />
                            </Form.Group>
                            <Form.Group as={Col} controlId="formGridprice" className="d-flex flex-column">
                                <Form.Label>Date</Form.Label>
                                <DatePicker
                                    selected={this.state.projection.date}
                                    onChange={this.setDate}
                                    showTimeSelect
                                    dateFormat="MMMM d, yyyy H:mm aa"
                                    timeIntervals={15}
                                    className="form-control"
                                />
                            </Form.Group>
                        </Form.Row>
                        <Form.Row>
                            <Form.Group as={Col} controlId="formCinema">
                                <Form.Label>Cinema</Form.Label>
                                {cinemaOptions}
                            </Form.Group>
                            <Form.Group as={Col} controlId="formCinemaHall">
                                <Form.Label>Cinema Hall</Form.Label>
                                {cinemaHallOptions}
                            </Form.Group>
                        </Form.Row>
                        <p className="validError">Error with creating projection. Check your input data!</p>
                        <Button variant="primary" type="submit" className="submit" >
                            Submit
                        </Button>
                    </Form>
                </Container>
            );
        }
    }
}
export default ProjectionForm;
