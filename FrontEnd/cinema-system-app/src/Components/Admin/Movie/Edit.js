import React from "react";
import { Link } from "react-router-dom";
import { Container,Col,Row } from "react-bootstrap";
import Loader from "./../../SmartGadgets/Loader";

class MovieEdit extends React.Component{
    state={movie:null};
    componentDidMount(){
        const movie = this.props.state.movie;
        this.setState({movie:movie});
    }
    render(){
        if(this.state.movie == null){
            return <Container><Loader></Loader></Container>;
        }else{
            return ("EDIT "+this.state.movie.name);
        }
    }
}
export default MovieEdit;