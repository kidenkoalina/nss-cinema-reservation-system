import React from "react";
import Form from "react-bootstrap/Form";
import { Col, Button, Row, Spinner } from "react-bootstrap";
import { Container } from "react-bootstrap";
import rules from "../../../Files/validationRules.json";
import {formValidation,validationFeedback,validationClassName} from "../../../Validator";
import { withRouter,Redirect } from "react-router-dom";
import Axios from "axios";
import {authHeader} from "../../../AuthServices/authHeader"
class MovieCreate extends React.Component{
    state = {
        form: {
            isValid: false,
            elements: {
                name: {
                    touched: false,
                    valid: false,
                    validationRules: rules.movie.name,
                },
                ageRate: {
                    touched: false,
                    valid: false,
                    validationRules: rules.movie.ageRate,
                },
                length: {
                    touched: false,
                    valid: false,
                    validationRules: rules.movie.length,
                },
                description: {
                    touched: false,
                    valid: false,
                    validationRules: rules.movie.description
                }
            },
        },
        movie: {
            name: null,
            ageRate: null,
            length: null,
            description: null
        }
    };
    inputUpdateHandler = async (event, nameOfFormInput, select) => {
        let newMovie = { ...this.state.movie };
         if (Object.keys(newMovie).includes(nameOfFormInput)) {
            if (select && event.target.value == 0) {
                newMovie[nameOfFormInput] = null;
            } else {
                newMovie[nameOfFormInput] = event.target.value;
            }
            await this.setState({ movie: newMovie });
        } 

        if (this.state.form.elements[nameOfFormInput].touched) {
            this.validateForm();
        }
        console.log(this.state);
    };

    validateForm = async () => {
        let newState = { ...this.state.form };
        formValidation(newState, this.state.movie);
        await this.setState({ form: newState });
    };

    submitHandler = async (event) => {
        event.preventDefault();
        console.log(this.state.movie);
        this.validateForm();
        let movie = {
            name: this.state.movie.name,
            length: this.state.movie.length,
            ageRate: this.state.movie.ageRate,
            description: this.state.movie.description,
            image: null,
        };
        const axios = Axios.create();


        const requestOptions = {
            method: 'POST',
            headers: authHeader()
        };
        axios.post('http://localhost:8080/movies',movie,requestOptions)
        .then((response) => {
            console.log(response);
            if(response.status == 201){
                //this.setState({registerSucces:true});
                alert("Movie Created!");
                //window.location.href = "/movies";
            }
        })
        .catch((error) => {
            console.log(error);
            console.error(error);
        });
    };

    render(){
        return(
            <>
                <Container className="register_container">
                    <Row>
                        <Col>
                            <div className="window midFull">
                                <h5>Create new movie</h5>
                                <Form
                                    className="registerUserForm mt-3 mb-3"
                                    onSubmit={this.submitHandler}>
                                    <Form.Row>
                                        <Form.Group as={Col}>
                                            <Form.Label>Name</Form.Label>
                                            <Form.Control
                                                maxLength="30"
                                                autoComplete="name"
                                                placeholder="Enter movie title"
                                                onChange={(event) =>
                                                    this.inputUpdateHandler(
                                                        event,
                                                        "name"
                                                    )
                                                }
                                                className={validationClassName(
                                                    "name",
                                                    this.state.form
                                                )}
                                            />
                                            <div className="invalid-feedback">
                                                {validationFeedback(
                                                    "name",
                                                    this.state.form
                                                )}
                                            </div>
                                        </Form.Group>
                                        <Form.Group as={Col}>
                                            <Form.Label>Length</Form.Label>
                                            <Form.Control type="number"
                                                min="1"
                                                placeholder="Enter length"
                                                onChange={(event) =>
                                                    this.inputUpdateHandler(
                                                        event,
                                                        "length"
                                                    )
                                                }
                                                className={validationClassName(
                                                    "length",
                                                    this.state.form
                                                )}
                                            />
                                            <div className="invalid-feedback">
                                                {validationFeedback(
                                                    "length",
                                                    this.state.form
                                                )}
                                            </div>
                                        </Form.Group>
                                    </Form.Row>
                                    <Form.Row>
                                        <Form.Group as={Col}>
                                            <Form.Label>Age rate</Form.Label>
                                            <Form.Control type="number"
                                                min="1"
                                                max="18"
                                                placeholder="Enter age rate"
                                                onChange={(event) =>
                                                    this.inputUpdateHandler(
                                                        event,
                                                        "ageRate"
                                                    )
                                                }
                                                className={validationClassName(
                                                    "ageRate",
                                                    this.state.form
                                                )}
                                            />
                                            <div className="invalid-feedback">
                                                {validationFeedback(
                                                    "ageRate",
                                                    this.state.form
                                                )}
                                            </div>
                                        </Form.Group>
                                    </Form.Row>
                                    <Form.Row>
                                        <Form.Group as={Col}>
                                            <Form.Label>Description</Form.Label>
                                            <Form.Control as="textarea"
                                                rows="3"
                                                placeholder="Description"
                                                onChange={(event) =>
                                                    this.inputUpdateHandler(
                                                        event,
                                                        "description"
                                                    )
                                                }
                                                className={validationClassName(
                                                    "description",
                                                    this.state.form
                                                )}
                                            />
                                            <div className="invalid-feedback">
                                                {validationFeedback(
                                                    "description",
                                                    this.state.form
                                                )}
                                            </div>
                                        </Form.Group>
                                    </Form.Row>
                                    <Form.Row>
                                        <Button
                                            className="submit"
                                            variant="primary"
                                            type="submit"
                                        >
                                            Create new movie
                                        </Button>
                                    </Form.Row>
                                </Form>
                            </div>
                        </Col>
                    </Row>
                </Container>
            </>
        );
    }
}
export default MovieCreate;