import React from "react";
import { Container,Col,Row } from "react-bootstrap";
import Loader from "./../../SmartGadgets/Loader";
import { Link } from "react-router-dom";
import {authHeader} from "../../../AuthServices/authHeader"
import Axios from "axios";
import Movie from "../Movie/Get";

class MovieTable extends React.Component{
    state={movies:null};
    componentDidMount(){
        this.fetchMovies();
    }
    fetchMovies(){
        let axios = Axios.create();
        axios.get("http://localhost:8080/movies")
        .then((response)=>{
            if(response.status == 200){
                this.setState({movies:response.data});
            }
        })
        .catch((error) => {
            console.log(error);
        })
    }
    render(){
        if(this.state.movies == null){
            return <Container><Loader></Loader></Container>;
        }else if(this.state.movies.length > 0){
            return <>
                    <Link to="/movies/create" className="submit mb-4" style={{float:"right"}}>Create new movie</Link>
                    <table className="window table center reservation">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>length</th>
                                <th>Age rate</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.movies.map((movie) => {
                                return <Movie key={movie.id} movie={movie} />;
                            })}
                        </tbody>
                    </table>
            </>
        }else{
            return <>
                    <p>No movies</p>
                </>
        }
    }
}
export default MovieTable;