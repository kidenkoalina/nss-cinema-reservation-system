import React from "react";
import { Redirect, Link } from "react-router-dom";
import { Container,Col,Row } from "react-bootstrap";
import MovieTable from "./Table";
class MovieIndex extends React.Component{
    
    render(){
        return (
            <Container className="mt-5">
                <MovieTable />
            </Container>
        )
    }
}
export default MovieIndex;