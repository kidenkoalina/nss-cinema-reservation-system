import React from "react";
import { Link } from "react-router-dom";

class Movie extends React.Component{
    state = {movie:null};
    componentDidMount(){
        const movie = this.props.movie;
        this.setState({movie:movie});
    }
    render(){
        if(this.state.movie == null){
            return <></>;
        }else{
            return (<tr>
                <td>{this.state.movie.name}</td>
                <td>{this.state.movie.length}</td>
                <td>{this.state.movie.ageRate}</td>
                <td>
                    <Link to={{
                        pathname: '/movies/edit',
                        state: {
                            movie: this.state.movie
                        }}} >Edit</Link>
                </td>
            </tr>);
        }
    }
}
export default Movie;