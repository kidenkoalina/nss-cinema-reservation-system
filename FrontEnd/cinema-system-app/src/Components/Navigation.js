import React from "react";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import NavDropdown from "react-bootstrap/NavDropdown";
import Form from "react-bootstrap/Form";
import FormControl from "react-bootstrap/FormControl";
import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";
import { Row, Col } from "react-bootstrap";
import { NavLink, Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import logo from "../Files/images/logo.png";
import {userService} from "../AuthServices/userService"

class Navigation extends React.Component {
    render() {
        const userLoggedIn = (username) => (
            
            <> 
            <Navbar.Text>
                Signed in as: <Link to="/user/profile">{username}</Link>
            </Navbar.Text>    
            {logoutItem}
            </>
        )
        const adminLogged = (admin) => (
            <>
            <NavDropdown title="Administration" id="basic-nav-dropdown">
                <NavDropdown.Item href="/movies/create">Create new movie</NavDropdown.Item>
                <NavDropdown.Item href="/projections/create">Add new projection</NavDropdown.Item>
                <NavDropdown.Divider />
                <NavDropdown.Item href="/dashboard">Dashboard</NavDropdown.Item>
                <NavDropdown.Item href="/movies">Movies</NavDropdown.Item>
                <NavDropdown.Item href="/projections">Projections</NavDropdown.Item>
                <NavDropdown.Item href="#action/3.3">Employes</NavDropdown.Item>
                <NavDropdown.Divider />
                <NavDropdown.Item href="/user/profile">User profile</NavDropdown.Item>
            </NavDropdown>
            {logoutItem}
            </>
        )

        const employeLogged = (employee) => (
            <>
            <NavDropdown title={employee} id="basic-nav-dropdown">
                <NavDropdown.Item href="/dashboard">Dashboard</NavDropdown.Item>
                <NavDropdown.Item href="/reservations">Show reservations</NavDropdown.Item>
                <NavDropdown.Item href="/projections">Show projections</NavDropdown.Item>
                <NavDropdown.Item href="#action/3.3">Employes</NavDropdown.Item>
                <NavDropdown.Divider />
                {/*<NavDropdown.Item href="/user/profile">User profile</NavDropdown.Item>*/}
            </NavDropdown>
            {logoutItem}
            </>
        )

        const logoutItem = (
            <Link to="/logout">
                <FontAwesomeIcon icon="sign-out-alt" />
            </Link>
        );


        const guestNavigation = (
                <>
                    <Link to="/login">
                        Login
                        <FontAwesomeIcon icon="user" />
                    </Link>
                
                    <Link to="/register">
                        Register
                        <FontAwesomeIcon icon="user" />
                    </Link>
                </>
        );

        let navigationItems = guestNavigation;
        if(userService.isLoggedIn()){
            const user = userService.getUser();
            console.log(user);
            if(user.role == "ADMIN" && user.admin){
                navigationItems = adminLogged(user.email);
            }else if(user.role == "EMPLOYEE"){
                navigationItems = employeLogged(user.email);
            }else{
                navigationItems = userLoggedIn(user.email);
            }
        }
        return (
            <header>
                <Container className="navigation">
                    <Navbar expand="lg" className="justify-content-between">
                        <Navbar.Brand>
                            <NavLink to="/"><img src={logo} className="logo" /></NavLink>
                        </Navbar.Brand>
                        <Nav>
                            {navigationItems}
                        </Nav>
                    </Navbar>
                </Container>
            </header>
        );
    }
}

export default Navigation;
