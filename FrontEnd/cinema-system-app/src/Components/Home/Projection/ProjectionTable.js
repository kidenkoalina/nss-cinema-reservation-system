import React from "react";
import { Container,Col,Row } from "react-bootstrap";
import ProjectionBanner from "./ProjectionBanner";
import CardColumns from "react-bootstrap/CardColumns";
import Spinner from "react-bootstrap/Spinner";
import Cookies from "universal-cookie";
import ButtonInRow from "../../SmartGadgets/ButtonInRow";
import Loader from "../../SmartGadgets/Loader";

import { authHeader } from '../../../AuthServices/authHeader';

class ProjectionTable extends React.Component {
    state = { projections: null, cinemas:null, selectedCinema:null, permission:null };
    componentDidMount() {
        const permission = this.props.permission;
        this.setState({permission:permission});
        //get projections
        this.fetchProjections();
        //getCinemas
        fetch(`http://localhost:8080/cinemas`,this.requestOptions())
        .then(response => response.json())
        .then((cinemas)=>{
            this.setState({ cinemas: cinemas});
            if(cinemas.length > 0){
                this.setState({selectedCinema:cinemas[0].name});
            }else{
                //neexistuji zadna kina
            }
            console.log(cinemas);
        })
        .catch((e) => console.error(e));
        
    }
    requestOptions = function() {
        return {
        method: 'GET',
        headers: authHeader()}
    };
    setCinema(e){
        if (e.target.selectedIndex != 0){
            const selectedCinema = e.target.value;
            console.log(selectedCinema);
            this.setState({selectedCinema:selectedCinema}, () => this.fetchProjections());
        }else{
            this.setState({selectedCinema:null}, () => this.fetchProjections());
        }
        //console.log(e.target.selectedIndex);
        
    }
    fetchProjections(){
        console.log("setted: "+this.state.selectedCinema);
        let req = `http://localhost:8080/projections`
        if(this.state.selectedCinema != null){
            req = `http://localhost:8080/projections/cinema/`+this.state.selectedCinema;
        }
        console.log(req);
        fetch(req,this.requestOptions())
        .then(response => response.json())
        .then((projections)=>{
            console.log(projections);
            this.setState({ projections: projections });
        })
        .catch((e) => console.error(e));
    }
    render() {
        let cinemaSelect = null;
        if(this.state.cinemas != null && this.state.cinemas.length > 0){
            console.log(this.state);
            cinemaSelect =  <select onChange={(event)=> this.setCinema(event)}>
                                <option>All cinemas</option>
                                {this.state.cinemas.map((cinema) => {
                                    return <option key={cinema.id}>{cinema.name}</option>
                                })}
                            </select>
        }
        if (this.state.projections === null) {
            return (
                <Container><Loader></Loader></Container>
            );
        } else {
            return (
                <Container className="projections">
                    <Row>
                        <Col><h4>Projections list</h4></Col>
                        <Col>{cinemaSelect}</Col>
                    </Row>
                    <table className="center">
                        <thead>
                            <tr>
                                <th>Cinema</th>
                                <th>Cinema hall</th>
                                <th>Movie</th>
                                <th>Date and Time</th>
                                <th>Price</th>
                                <th>More info</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.projections.map((projection) => {
                                return (
                                    <ProjectionBanner permission={this.state.permission} key={projection.id} projection={projection} />
                                );
                            })}
                        </tbody>
                    </table>
                </Container>
            );
        }
    }
}

export default ProjectionTable;
