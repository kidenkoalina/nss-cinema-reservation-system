import React from "react";
import { Link } from "react-router-dom";

class ProjectionBanner extends React.Component{
    constructor(props){
        super();
        this.projection = props.projection;
        console.log(this.projection);
        this.permission = props.permission;
    }

    render(){
        let action = null;
        if(this.permission){
            action = (<Link to={{
                pathname: '/reservations',
                state: {
                    projection: this.projection
                }
            }}
            >Show reservations
            </Link>);
        }else{
            action = (<Link to={{
                pathname: '/reservation/new',
                state: {
                    projection: this.projection
                }
                
            }}
            >Buy
            </Link>);
        }
    return(
            
            <tr className="projectionBanner">
            <td>{this.projection.cinemahall.cinema.name}</td>
            <td>{this.projection.cinemahall.number}</td>
            <td>{this.projection.movie.name}</td>
            <td>{this.projection.date} at {this.projection.time.replace(/\.\d+/, "")}</td>
            <td>{this.projection.price},-</td>
            <td>
                {action}
            </td>
            </tr>
    );
    }
}
export default ProjectionBanner;