import React from "react";
import { Link } from "react-router-dom";
import img from "../../../Files/images/tenet.jpg"
import  logo from "../../../Files/images/inception.jpg";

class MovieBanner extends React.Component{
    constructor(props){
        super();
        this.movie = props.movie;
    }

    redirectToMovieDetail(event){
        console.log(event.target);
        window.location("/movies/"+this.props.movie.id);
    }

    render(){
        return(
            <div className="movieBanner">
                <div className="movieImage">
                    <img src={logo}/>
                </div>
                <div className="movieInfo">
                    <h5>{this.movie.name}</h5>
                    <span>Description:</span>
                    <p className="desc">{this.movie.description}</p>
                    <span>Length:</span>
                    <p>{this.movie.length} minutes</p>
                    {/*<Link onClick={(event) => this.redirectToMovieDetail(event)} to={"/movies/" + this.props.movie.id}>Show more</Link>*/}
                </div>
            </div>);
    }
}
export default MovieBanner;