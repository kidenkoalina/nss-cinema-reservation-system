import React from "react";
import Card from "react-bootstrap/Card";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Button, Row, Col, Container, Image, ListGroup } from "react-bootstrap";
import { Link } from "react-router-dom";
import axios from "axios";
import Spinner from "react-bootstrap/Spinner";
import { Form,Modal } from "react-bootstrap";
import AchievmentModal from "../../SmartGadgets/AchievementModal";
import AchievementListInline from "../../SmartGadgets/AchievementListInline";

class MovieDetail extends React.Component {
    /*state = { trip: null, selectedSession : {id:null, from_date:null,to_date:null, price:null}};
    user = null;
    formIsValid = false;
    static contextType = appContext;
    userDTO = {achievements_special:null,certifications:null,achievements_categorized:null,level:1};
    sessionsIds = [];*/
    
    state = {movie: null}
    async componentDidMount() {
        console.log(this.props);
        const response = await fetch(
            `http://localhost:8080/movies/` + this.props.match.params.id
        );
        const data = await response.json();
        console.log(data);
        this.setState({movie: data});
    }
    render() {
        if (this.state.movie === null) {
            return (
                <Container className="p-5">
                    <Spinner animation="border" role="status">
                        <span className="sr-only">Loading...</span>
                    </Spinner>
                </Container>
            );
        } else {
            return (
                <Container id="movieDetail">
                        <div className="movieBanner">
                            <h5>Movie name: {this.state.movie.name}</h5>
                            <p>Description: {this.state.movie.description}</p>
                            <p>Length: {this.state.movie.length}</p>
            <p>Age: {this.state.movie.ageRate}</p>
                        </div>
                </Container>
            );
        }
    }
}
export default MovieDetail;
