import React from "react";
import { Container } from "react-bootstrap";
import MovieBanner from "./MovieBanner";
import CardColumns from "react-bootstrap/CardColumns";
import Cookies from "universal-cookie";
import Slider from '@farbenmeer/react-spring-slider';
import Loader from "../../SmartGadgets/Loader";
import ProjectionTable from "../../Home/Projection/ProjectionTable";

class Index extends React.Component {
    async componentDidMount() {
        const response = await fetch(`http://localhost:8080/movies`);
        const data = await response.json();
        console.log(data);
        this.setState({ movies: data });
    }
    state = { movies: null, projections: null};

    render() {
        let movieSLider = null;
        if (this.state.movies === null) {
            movieSLider = <Container><Loader></Loader></Container>
        } else {
            movieSLider =  
                (<div className="movieSlider">
                    <Container>
                    <h4>Now playing</h4>
                    <div className="movies">
                        <Slider auto={5000} hasBullets > 
                            {this.state.movies.map((movie) => {
                                return (
                                    <MovieBanner key={movie.id} movie={movie} />
                                );
                            })
                            }
                        </Slider>
                    </div>
                    </Container>
                </div>);
        }
        
        return(
            <>
                {movieSLider}
                <ProjectionTable/>
            </>
        );
    }
}

export default Index;
