
import { authHeader } from './authHeader';
import Axios from 'axios';

export const userService = {
    login,
    logout,
    isLoggedIn,
    getUser,
    loadUser
};

async function login(username, password) {
    const axios = Axios.create();
    //create params for request
    const params = new URLSearchParams();
    params.append('username', username);
    params.append('password', password);
    //define headers for request
    const headers = { 'content-type': 'application/x-www-form-urlencoded;charset=utf-8' };
    return axios.post('http://localhost:8080/j_spring_security_check', params,{headers:headers})
        .then(handleResponse)
        .then(user => {
            return Promise.resolve(user);
        })
        .catch((error)=>{ 
            console.error(error);
            return Promise.reject(error);
        });
}
// remove user from local storage to log user out
async function logout() {
    const axios = Axios.create();
    return axios.get(`http://localhost:8080/logout`)
        .then((response) => {
            localStorage.removeItem('user');
            return Promise.resolve(response);
        });
}
function getUser(){
    return JSON.parse(localStorage.getItem('user'));
}
async function loadUser(authdata){
    const requestOptions = {
        method: 'GET',
        headers: { 'Authorization': 'Basic ' + authdata }
    };
    const axios = Axios.create();
    return new Promise((resolve,reject) => {
        axios.get('http://localhost:8080/user/current',requestOptions)
        .then((response) =>{
            //console.log(response);
            if(response.data){
                const user = response.data;
                user.authdata = authdata;
                resolve(user);
            }
        })
        .catch((error)=>{
            reject(error);
        })
    })
}
function isLoggedIn(){
    return !(localStorage.getItem('user') === null);
}
function handleResponse(response) {
    if (!response.data.loggedIn) {
        if (response.status === 401) {
            logout();
            window.location.reload(true);
        }
        const error = (response.data.errorMessage);
        return Promise.reject(error);
    }
    return Promise.resolve(response.data);
}