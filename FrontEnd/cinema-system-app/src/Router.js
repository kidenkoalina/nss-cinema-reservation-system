import React from "react";
import Home from "./Components/Home/Movie/Index";
import Logout from "./Components/Auth/Logout";
import Login from "./Components/Auth/Login";
import Register from "./Components/Register";
import ReservationForm from "./Components/Reservation/Create";
import MovieDetail from "./Components/Home/Movie/Detail"
import { Redirect, Switch, Route } from "react-router-dom";
import ProjectionForm from "./Components/Admin/Projection/Create"
import {userService} from "./AuthServices/userService"
import RegisterForm from "./Components/Reservation/Create";
import NotFound from "./Components/ErrorHandlers/NotFound";
import Reservation from "./Components/Employee/Reservations";
import MyAlert from "./Components/SmartGadgets/MyAlert";
import Forbidden from "./Components/ErrorHandlers/Forbidden";
import SuccessMessage from "./Components/SmartGadgets/SuccessMessage";
import UserProfile from "./Components/Profile/Index";
import UserSettings from "./Components/Profile/Settings";
import ProjectionTable from "./Components/Home/Projection/ProjectionTable";
import EmployeeHome from "./Components/Employee/Home";
import MovieIndex from "./Components/Admin/Movie/Index";
import MovieEdit from "./Components/Admin/Movie/Edit";
import MovieCreate from "./Components/Admin/Movie/Create";
//import MovieIndex from "./Components/Admin/Movie/Index";
function Router(props) {

    const ROLE_EMPLOYEE = "EMPLOYEE";
    const ROLE_ADMIN = "ADMIN";
    const ROLE_USER = "USER";

    const allowAuth = (component,message = <MyAlert variant="danger" text="" heading="You must be logged in!"/>) => {
        console.log(component);
        if (userService.isLoggedIn()) {
            console.log("USER JE PRIHLASENY");
            return component;
        } else {
            console.log("USER NENI PRIHLASENY");
            return <Redirect to={{ pathname: "/login",message:message}} />;
        }
    };

    const allowAuthWithRole = (component, role,message = <MyAlert variant="danger" text="" heading="You must be logged in!"/>) => {
        console.log("ASDASDASDADJKASDASKLDJASLKJDLASKDSADLAS");
        console.log(component);
        if (userService.isLoggedIn()) {
            const user = userService.getUser();
            if (user.role === role || user.role === ROLE_ADMIN) {
                return component;
            } else {
                return <Redirect to={{ pathname: "/forbidden" }} />;
            }
        } else {
            return <Redirect to={{ pathname: "/login",message:message }} />;
        }
    };

    const allowGuest = (component) => {
        //userService.logout();
        console.log(userService.isLoggedIn());
        //console.log()
        if (!userService.isLoggedIn()) {
            return component;
       } else {
            return <Redirect to={{ pathname: "/" }} />;
       }
    };

    return (
        <div>
            <Switch>
                {/* Authorization */}
                <Route path="/login" exact={true} render={() => {return allowGuest(<Login />); }}/>
                <Route path="/logout" render={() => {return allowAuth(<Logout />, <MyAlert heading="You have been successfully logged out!"/>);}}/>
                {/* User Register */}
                <Route path="/register" exact={true} render={() => {return allowGuest(<Register />);}}/>
                <Route path="/register/success" component={SuccessMessage}/>
                {/* Home */}
                <Route path="/" exact component={Home} />

                {/* Movies */}
                <Route path="/movies" exact={true} render={() => {return allowAuthWithRole(<MovieIndex />,ROLE_ADMIN)}} />
                <Route path="/movies/create" render={() => {return allowAuthWithRole(<MovieCreate />,ROLE_ADMIN)}} />
                <Route path="/movies/edit" render={(state) => {return allowAuthWithRole(<MovieEdit state={state.location.state}/>,ROLE_ADMIN)}} />
                <Route path="/movies/:id" exact={true} component={MovieDetail} />

                {/* Employee dashboard */}
                <Route path="/dashboard" exact={true} render={() => {return allowAuthWithRole(<EmployeeHome />,ROLE_EMPLOYEE)}} />

                {/* Reservation */}
                <Route path="/reservations" render={(state) => {return allowAuthWithRole(<Reservation state={state.location.state}/>,ROLE_EMPLOYEE)}} />
                <Route path="/reservation/new" render={(props) => {return allowAuth(<ReservationForm state={props.location.state} />)}} />
                <Route path="/reservation/success" component={SuccessMessage}/>

                {/* User Profile */}
                <Route path="/user/profile" exact render={() => {return allowAuth(<UserProfile />);}} />
                <Route path="/user/settings" render={() => {return allowAuth(<UserSettings/>);}}/>

                {/* projections */}
                <Route path="/projections/create" render={() => {return allowAuthWithRole(<ProjectionForm />, ROLE_ADMIN);}}/>
                <Route path="/projections" component = {ProjectionTable}/>

                {/* 404 & 403 errors*/ }
                <Route path="/forbidden" component={Forbidden}/>
                <Route component={NotFound} />
            </Switch>
        </div>
    );
}
export default Router;
