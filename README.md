# Semestrální práce z předmětu NSS
**Téma:** Cinema System

**Vypracovali:**
* Alina Kidenko
* Anna Vičíková
* Jiří Štěpán


## Funkcionality
### Databáze
Databáze je realizována přes PostgreSQL. Přihlašovací údaje k databázi se nachází v application.properties.
### Cache
Ke cachování je využit Hazelcast, který je naimplementován v /rest/MovieController a nakonfigurován v /config/HazelcastConfig. Cachují se filmy.
### Bezpečnost
Aplikace je zabezpečena pomocí Basic Authorization. To je nakonfigurováno v /config/SecurityConfig
### Interceptors
Třída interceptors je využívána k logování, jak dlouho trvalo vyřízení požadavku. Je nakonfigurována pomocí /interceptors/InterceptorAppConfig a implementována /interceptors/RequestTimeInterceptor.
### Kafka
V projektu je využita distribuovaná streamovací platforma Kafka. Consumer a Producer se nacházejí v /kafka/service. Controller, který přijímá zprávy, se nachází v /kafka/service/controller/KafkaController.
### ElasticSearch
Fulltextový vyhledávač je implementován pro uživatele, který je stvořený právě pouze pro tento nástroj. ElasticSearch je nakonfigurován v application.properties, využívá tzv. „Document“ v /elastics/PersonElastic, repozitář /elastics/PersonRepo a controller v /elastics/PersonElasticController, který podporuje vytváření osoby, získání osoby podle jeho ID, získání všech osob, update osoby podle ID a smazání osoby.
### Design Patterny
#### Dependency Injection
DI se v projektu nachází při předávání dependency servisy v controllerech.
#### Façade
Interface pro tento design pattern se nachází v /service/facade/ProjectionServiceFacade a jeho implementace v /service/facade/ProjectionServiceFacadeImpl. Využívá se v Projection controlleru (nacházející se v /rest/ProjectionController), který využívá 5 určitých servis.  
#### Factory
Využit jako DAO pattern. Všechny DAO třídy lze nalézt ve složce /dao. 
#### Builder
Tento design pattern je využit jako staticky builder pro Response Entity, lze ho nalézt v /rest/EmployeeController. 








