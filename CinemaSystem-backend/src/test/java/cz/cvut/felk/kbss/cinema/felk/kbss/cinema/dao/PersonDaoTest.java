package cz.cvut.felk.kbss.cinema.felk.kbss.cinema.dao;

import cz.cvut.felk.kbss.cinema.SpringbootDemoApplication;
import cz.cvut.felk.kbss.cinema.dao.PersonDao;
import cz.cvut.felk.kbss.cinema.felk.kbss.cinema.environment.Generator;
import cz.cvut.felk.kbss.cinema.model.Person;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
@ComponentScan(basePackageClasses = SpringbootDemoApplication.class)
public class PersonDaoTest {

    @PersistenceContext
    private EntityManager em;

    @Autowired
    private PersonDao personDao;

    @Test
    public void findByEmailReturnsPersonWithMatchingEmail() {
        final Person person = Generator.generatePerson();
        em.persist(person);

        final Person result = personDao.findByEmail(person.getEmail());
        assertNotNull(result);
        assertEquals(person.getId(), result.getId());
    }

    @Test
    public void findByUsernameReturnsNullForUnknownUsername() {
        assertNull(personDao.findByEmail("unknownUsername"));
    }
}
