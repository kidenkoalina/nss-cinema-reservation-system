package cz.cvut.felk.kbss.cinema.felk.kbss.cinema.environment;

import cz.cvut.felk.kbss.cinema.model.*;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * 10.cviko
 */
public class Generator {

    @Autowired
    EntityManager em;

    private static final Random RAND = new Random();

    public static int randomInt() {
        return RAND.nextInt();
    }

    public static boolean randomBoolean() {
        return RAND.nextBoolean();
    }

    public static Person generatePerson() {
        final Person person = new Person();
        person.setName("Name" + randomInt());
        person.setSurname("Surname" + randomInt());
        person.setEmail("username" + randomInt() + "@kbss.felk.cvut.cz");
        person.setPassword(Integer.toString(randomInt()));
        person.setRole(Role.PERSON);
        return person;
    }

//    public static Movie generateMovie(){
//        final Movie movie = new Movie();
//        movie.setName("TestMovie" + randomInt());
//        movie.setDescription("TestDescription" + randomInt());
//        movie.setAgeRate((short) 6);
//        movie.setLength((short) 120);
//        return movie;
//    }

//    public static List<Projection> generateProjections(Movie movie){
//        List<Projection> projections = new ArrayList<>();
//        for (int i = 0; i < 10; i++) {
//                projections.add(generateProjection(movie));
//        }
//        return  projections;
//    }

//    public static Projection generateProjection(Movie movie){
//        final Projection projection = new Projection();
//        projection.setMovie(movie);
//        projection.setDabing("CZ");
//        projection.setDate(LocalDate.now().plusDays(15));
//        projection.setTime(LocalTime.now());
//        projection.setCinemahall(generateCinemahall());
//        return projection;
//    }

//    public static Cinemahall generateCinemahall(){
//        final Cinemahall cinemahall = new Cinemahall();
//        cinemahall.setNumber((short) 1);
//        cinemahall.setCinema(generateCinema());
//        return cinemahall;
//    }

//    public static Cinema generateCinema(){
//        final Cinema cinema = new Cinema();
//        cinema.setName("Cinema" + randomInt());
//        cinema.setTown("Town");
//        return cinema;
//    }
}
