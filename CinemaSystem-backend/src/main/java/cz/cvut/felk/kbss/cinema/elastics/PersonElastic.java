package cz.cvut.felk.kbss.cinema.elastics;

import com.fasterxml.jackson.annotation.JsonIgnore;
import cz.cvut.felk.kbss.cinema.model.Reservation;
import cz.cvut.felk.kbss.cinema.model.Role;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlTransient;
import java.util.List;

@Document(indexName = "persons", type="default")
public class PersonElastic {

    @Id
    private Integer id;
    private String name;
    private String surname;
    private String email;
    private String password;
    private Role role;
    private List<Reservation> reservationList;

    public PersonElastic() {}

    public PersonElastic(@NotNull @Size(min = 1, max = 50) String name, @NotNull @Size(min = 1, max = 50) String surname,
                         @NotNull @Size(min = 1, max = 100) String email, @NotNull @Size(min = 1, max = 100) String password,
                         Role role, List<Reservation> reservationList) {
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.password = password;
        this.role = role;
        this.reservationList = reservationList;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }
    //security //opravdu to nejde jinak?
    public String getUsername() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public void setAdmin(Boolean admin) {
        this.role = Role.ADMIN;
    }

    public boolean isAdmin(){ return this.role == Role.ADMIN; }

    public boolean isEmployee() {return this.role == Role.EMPLOYEE;}

    public boolean isPerson() {return this.role == Role.PERSON;}

    public void erasePassword() {
        this.password = null;
    }

    public void encodePassword(PasswordEncoder encoder) {
        this.password = encoder.encode(password);
    }


    @XmlTransient
    public List<Reservation> getReservationList() {
        return reservationList;
    }

    public void setReservationList(List<Reservation> reservationList) {
        this.reservationList = reservationList;
    }

    @Override
    public String toString() {
        return "Person{" +
                name + " " + surname +
                "(" + email + ")}";
    }
}
