package cz.cvut.felk.kbss.cinema.dao;

import cz.cvut.felk.kbss.cinema.model.Employee;
import cz.cvut.felk.kbss.cinema.model.Person;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class EmployeeDao extends BaseDao<Employee>{
    public EmployeeDao(){
        super(Employee.class);
    }

    public List<Employee> findAll() {
        try {
            List<Employee> employees = new ArrayList<>();
            List<Employee> employees1 = em.createNamedQuery("Employee.findAll", Employee.class).getResultList();
            for (Employee p : employees1){
                if(p.isEmployee() || p.isAdmin()) employees.add(p);
            }
            return employees;
        } catch (NoResultException e) {
            return null;
        }
    }

    public Employee findByEmail(String email) {
        try {
            return em.createNamedQuery("Employee.findByEmail", Employee.class).setParameter("email", email)
                    .getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }
}
