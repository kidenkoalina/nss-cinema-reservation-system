package cz.cvut.felk.kbss.cinema.rest;

import cz.cvut.felk.kbss.cinema.exception.NotFoundException;
import cz.cvut.felk.kbss.cinema.exception.ValidationException;
import cz.cvut.felk.kbss.cinema.model.Movie;
import cz.cvut.felk.kbss.cinema.model.Projection;
import cz.cvut.felk.kbss.cinema.model.Reservation;
import cz.cvut.felk.kbss.cinema.rest.util.RestUtils;
import cz.cvut.felk.kbss.cinema.service.facade.ProjectionServiceFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/projections")
public class ProjectionController {

    private static final Logger LOG = LoggerFactory.getLogger(ProjectionController.class);
    private final ProjectionServiceFacade psf;

    @Autowired
    public ProjectionController(ProjectionServiceFacade psf) {
        this.psf = psf;
    }


    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Projection> getProjections() { return psf.getProjections(); }

    @GetMapping(value = "/current", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Projection> getCurrentProjections(){
        LocalDate today = LocalDate.now();
        return psf.getProjections(today);
    }

    //OTESTOVAT
    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Projection getProjection(@PathVariable Integer id){
        return psf.getProjection(id);
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_EMPLOYEE')")
    @GetMapping(value = "/{id}/reservations", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Reservation> getReservationsByProjection(@PathVariable Integer id){
        return psf.getReservations(id);
    }

    @GetMapping(value = "/date/{date}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Projection> getProjectionsByDate(@PathVariable String date){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse(date, formatter);
        return psf.getProjections(localDate);
    }

    @GetMapping(value = "/cinema/{cinemaName}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Projection> getProjectionsByCinema(@PathVariable String cinemaName){
        return psf.getProjectionsByCinema(cinemaName);
    }

    @GetMapping(value = "/movie/{movieName}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Projection> getProjectionsByMovie(@PathVariable String movieName){
        return psf.getProjectionsByMovie(movieName);
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_EMPLOYEE')")
    @GetMapping(value = "/{id}/statistics", produces = MediaType.APPLICATION_JSON_VALUE)
    public String getStatisticsByProjection(@PathVariable Integer id){
        return psf.getStatisticsByProjection(id);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> addNewProjection(@RequestBody Projection p){
        int id = psf.addNewProjection(p);
        final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/{id}", id);
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateProjection(@PathVariable Integer id, @RequestBody Projection projection) {
        psf.updateProjection(id, projection);
        LOG.debug("Updated projection {}.", projection);
    }


    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @DeleteMapping(value = "/{id}/cancelUnconfirmedReservations")
    @ResponseStatus(HttpStatus.OK)
    public void cancelUnconfirmedReservationsByProjection(@PathVariable Integer id){
        psf.cancelUnconfirmedReservationsByProjection(id);
    }
}
