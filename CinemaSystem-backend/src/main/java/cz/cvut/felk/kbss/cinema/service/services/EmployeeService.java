package cz.cvut.felk.kbss.cinema.service.services;

import cz.cvut.felk.kbss.cinema.dao.EmployeeDao;
import cz.cvut.felk.kbss.cinema.model.Employee;
import cz.cvut.felk.kbss.cinema.model.Person;
import cz.cvut.felk.kbss.cinema.model.Role;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;

@Service
public class EmployeeService {

    private final EmployeeDao employeeDao;
    private final PasswordEncoder passwordEncoder;

    public EmployeeService(EmployeeDao employeeDao, PasswordEncoder passwordEncoder) {
        this.employeeDao = employeeDao;
        this.passwordEncoder = passwordEncoder;
    }

    @Transactional(readOnly = true)
    public List<Employee> findAll(){
        return employeeDao.findAll();
    }

    @Transactional
    //can create employees and admins
    public void persist(Employee employee) {
        Objects.requireNonNull(employee);
        employee.encodePassword(passwordEncoder);
        //if you forgot to set Role.ADMIN in POST request body, then you will create just employee
        if (employee.getRole() == null) {
            employee.setRole(Role.EMPLOYEE);
        }
        employeeDao.persist(employee);
    }

    @Transactional
    public void update(Employee employee) {
        employeeDao.update(employee);
    }

    @Transactional(readOnly = true)
    public Employee find(Integer id) {
        return employeeDao.find(id);
    }

    @Transactional(readOnly = true)
    public boolean exists(String email) {
        return employeeDao.findByEmail(email) != null;
    }
}