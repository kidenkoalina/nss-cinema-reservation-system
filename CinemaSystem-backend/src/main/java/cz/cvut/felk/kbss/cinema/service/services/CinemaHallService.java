package cz.cvut.felk.kbss.cinema.service.services;

import cz.cvut.felk.kbss.cinema.dao.CinemaDao;
import cz.cvut.felk.kbss.cinema.dao.CinemaHallDao;
import cz.cvut.felk.kbss.cinema.model.Cinema;
import cz.cvut.felk.kbss.cinema.model.Cinemahall;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;

@Service
public class CinemaHallService {

    private final CinemaHallDao cinemaHallDao;

    @Autowired
    public CinemaHallService(CinemaHallDao cinemaHallDao){this.cinemaHallDao = cinemaHallDao;}

    @Transactional(readOnly = true)
    public List<Cinemahall> findAll(){
        return cinemaHallDao.findAll();
    }

    public List<Cinemahall> findByCinema(Cinema cinema){
        return cinemaHallDao.findByName(cinema);
    }

    @Transactional
    public void persist(Cinemahall cinemaHall) {
        Objects.requireNonNull(cinemaHall);
        cinemaHallDao.persist(cinemaHall);
    }

    @Transactional(readOnly = true)
    public Cinemahall find(Integer id) {
        return cinemaHallDao.find(id);
    }


    @Transactional
    public void update(Cinemahall cinemahall) {
        cinemaHallDao.update(cinemahall);
    }

    @Transactional
    public void remove(Cinemahall cinemahall){
        Objects.requireNonNull(cinemahall);
        cinemaHallDao.remove(cinemahall);
    }
}
