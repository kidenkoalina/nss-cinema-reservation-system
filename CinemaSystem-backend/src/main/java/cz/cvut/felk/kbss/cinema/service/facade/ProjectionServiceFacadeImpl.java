package cz.cvut.felk.kbss.cinema.service.facade;

import cz.cvut.felk.kbss.cinema.exception.NotFoundException;
import cz.cvut.felk.kbss.cinema.exception.ValidationException;
import cz.cvut.felk.kbss.cinema.model.*;
import cz.cvut.felk.kbss.cinema.service.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;

@Component
public class ProjectionServiceFacadeImpl implements ProjectionServiceFacade {

    private final ProjectionService projectionService;
    private final ReservationService reservationService;
    private final CinemaService cinemaService;
    private final MovieService movieService;
    private final CinemaHallService cinemaHallService;


    @Autowired
    public ProjectionServiceFacadeImpl(ProjectionService projectionService, ReservationService reservationService, CinemaService cinemaService, MovieService movieService, CinemaHallService cinemaHallService) {
        this.projectionService = projectionService;
        this.reservationService = reservationService;
        this.cinemaService = cinemaService;
        this.movieService = movieService;
        this.cinemaHallService = cinemaHallService;
    }


    @Override
    public List<Projection> getProjections() {
        return projectionService.findAll();
    }

    @Override
    public List<Projection> getProjections(LocalDate date) {
        return projectionService.findByDate(date);
    }

    @Override
    public Projection getProjection(Integer id) {
        Projection projection = projectionService.find(id);
        if (projection == null) {
            throw NotFoundException.create("Projection", id);
        }
        return projection;
    }

    @Override
    public List<Reservation> getReservations(Integer id) {
        final Projection projection = projectionService.find(id);
        if (projection == null) {
            throw NotFoundException.create("Projection", id);
        }
        return reservationService.findByProjection(projection);
    }

    @Override
    public List<Projection> getProjectionsByCinema(String cinemaName) {
        final Cinema cinema = cinemaService.findByName(cinemaName);
        return projectionService.findByCinema(cinema);
    }

    @Override
    public List<Projection> getProjectionsByMovie(String movieName) {
        final Movie movie = movieService.findByName(movieName);

        return projectionService.findByMovie(movie);
    }

    @Override
    public String getStatisticsByProjection(Integer id) {
        final Projection projection = projectionService.find(id);
        if (projection != null) {
            final List<Reservation> reservations = reservationService.findByProjection(projection);
            Integer ticketsSum = 0;
            for (Reservation r : reservations) {
                if (r.isConfirmed()) {
                    ticketsSum += r.getTickets();
                }
            }
            return "Number of sold tickets is " + ticketsSum + " !";
        }
        throw NotFoundException.create("Projection", id);
    }

    @Override
    public int addNewProjection(Projection projection) {
        // Check if cinemahall and movie exists
        Cinemahall ch = projection.getCinemahall();
        Movie m = projection.getMovie();
        if (cinemaHallService.find(ch.getId()) == null) {
            throw NotFoundException.create("CinemaHall", ch.getId());
        }
        if (movieService.find(m.getId()) == null){
            throw NotFoundException.create("Movie", m.getId());
        }

        //if exists, then persist a new projection
        projectionService.persist(projection);
        return projection.getId();

}

    @Override
    public void updateProjection(Integer id, Projection projection) {
        final Projection original = getProjection(id);
        if (!original.getId().equals(projection.getId())) {
            throw new ValidationException("Projection identifier in the data does not match the one in the request URL.");
        }
        projectionService.update(projection);

    }

    @Override
    public void cancelUnconfirmedReservationsByProjection(Integer id) {
        Projection projection = projectionService.find(id);
        if (projection == null) {
            throw NotFoundException.create("Projection", id);
        }
        reservationService.cancelUnconfirmedReservation(projection);
    }
}
