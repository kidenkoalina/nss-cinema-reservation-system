package cz.cvut.felk.kbss.cinema.model;

public enum Role {
    PERSON("ROLE_PERSON"),
    ADMIN("ROLE_ADMIN"),
    EMPLOYEE("ROLE_EMPLOYEE");

    private final String name;

    Role(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
