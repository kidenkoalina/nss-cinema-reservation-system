package cz.cvut.felk.kbss.cinema.rest;

import cz.cvut.felk.kbss.cinema.exception.NotFoundException;
import cz.cvut.felk.kbss.cinema.exception.ValidationException;
import cz.cvut.felk.kbss.cinema.model.Employee;
import cz.cvut.felk.kbss.cinema.model.Person;
import cz.cvut.felk.kbss.cinema.rest.util.RestUtils;
import cz.cvut.felk.kbss.cinema.security.model.AuthenticationToken;
import cz.cvut.felk.kbss.cinema.service.services.EmployeeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.security.Principal;
import java.util.List;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/employees")
public class EmployeeController {

    private static final Logger LOG = LoggerFactory.getLogger(PersonController.class);

    private final EmployeeService employeeService;

    @Autowired
    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    //READ ALL EMPLOYEES
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Employee> getEmployees(){
        return employeeService.findAll();
    }

    //GET 1 EMPLOYEE
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Employee getEmployee(@PathVariable Integer id) {
        final Employee employee = employeeService.find(id);
        if (employee == null) {
            throw NotFoundException.create("Employee", id);
        }
        return employee;
    }

    //CREATE EMPLOYEE
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> registerEmployee(@RequestBody Employee e){
        employeeService.persist(e);
        LOG.debug("Person {} successfully registered.", e);
        final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/{id}", e.getId());
//        final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/current");

        //using static builder for ResponseEntity
        //don't know what is the best choice from next two options:
        return ResponseEntity.ok()
                .headers(headers)
                .build();
        //OR
//        ResponseEntity responseEntity = ResponseEntity.ok()
//                .headers(headers)
//                .body(e);
//        return responseEntity;
    }

    //TODO think about it one more time :D this method is already in personController
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_PERSON', 'ROLE_EMPLOYEE')")
    @GetMapping(value = "/current", produces = MediaType.APPLICATION_JSON_VALUE)
    public Person getCurrent(Principal principal) {
        final AuthenticationToken auth = (AuthenticationToken) principal;
        return auth.getPrincipal().getUser();
    }

    //UPDATE EMPLOYEE
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateEmployee(@PathVariable Integer id, @RequestBody Employee employee) {
        final Employee original = getEmployee(id);
        if (!original.getId().equals(employee.getId())) {
            throw new ValidationException("Employee identifier in the data does not match the one in the request URL.");
        }
        employeeService.update(employee);
        LOG.debug("Updated employee {}.", employee);
    }
}
