/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.felk.kbss.cinema.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "cinemahall")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Cinemahall.findAll", query = "SELECT c FROM Cinemahall c")
    , @NamedQuery(name = "Cinemahall.findByNumber", query = "SELECT c FROM Cinemahall c WHERE c.number = :number")
    , @NamedQuery(name = "Cinemahall.findByCinema", query = "SELECT c FROM Cinemahall c WHERE :cinema = c.cinema")})
public class Cinemahall implements Serializable {

    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    private Integer id;

    @NotNull
    @Column(name = "number")
    private short number;

    @NotNull
    @Column(name = "capacity")
    private short capacity;

    @JoinColumn(name = "cinema", referencedColumnName = "id")
    @ManyToOne()
    private Cinema cinema;

    public Cinemahall(){}

    public Cinemahall(@NotNull short number, @NotNull short capacity, Cinema cinema) {
        this.number = number;
        this.capacity = capacity;
        this.cinema = cinema;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public short getNumber() {
        return number;
    }

    public void setNumber(short number) {
        this.number = number;
    }

    public short getCapacity() {
        return capacity;
    }

    public void setCapacity(short capacity) {
        this.capacity = capacity;
    }

    public Cinema getCinema() {
        return cinema;
    }

    public void setCinema(Cinema cinema) {
        this.cinema = cinema;
    }

    @Override
    public String toString() {
        return "Cinemahall{" +
                "number = '" + number + "', cinema = " + cinema.getName() +
                "}";
    }


}
