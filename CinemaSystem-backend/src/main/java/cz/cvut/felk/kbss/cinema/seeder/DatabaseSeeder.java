package cz.cvut.felk.kbss.cinema.seeder;

import cz.cvut.felk.kbss.cinema.dao.*;
import cz.cvut.felk.kbss.cinema.model.*;
import cz.cvut.felk.kbss.cinema.service.services.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalTime;

@Component
public class DatabaseSeeder implements ApplicationListener<ContextRefreshedEvent> {

    PersonDao personDao;
    CinemaDao cinemaDao;
    CinemaHallDao cinemaHallDao;
    MovieDao movieDao;
    ProjectionDao projectionDao;
    ReservationDao reservationDao;
    PersonService personService;

    @Autowired
    public DatabaseSeeder(PersonDao personDao, CinemaDao cinemaDao, CinemaHallDao cinemaHallDao, MovieDao movieDao,
                          ProjectionDao projectionDao, PersonService personService, ReservationDao reservationDao) {
        this.personDao = personDao;
        this.cinemaDao = cinemaDao;
        this.cinemaHallDao = cinemaHallDao;
        this.movieDao = movieDao;
        this.projectionDao = projectionDao;
        this.personService = personService;
        this.reservationDao = reservationDao;
    }

    @Override
    @Transactional
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        System.out.println("App is running...");

        //creating people
        createPersons();
        createAdmin();
        createEmployees();

        createCinemasAndCinemaHalls();
        createMovies();
        createProjections();
        createReservation();
    }

    private void createReservation() {
        //reservation by user, 2 tickets
        Person person = personDao.findAll().get(0);
        Projection projection = projectionDao.findAll().get(0);
        Reservation reservation = new Reservation(person,  projection, (short)5);
        reservationDao.persist(reservation);
    }

    private void createProjections() {

        //projection is in a 5 day and 60 minutes
        LocalDate date = LocalDate.now().plusDays(5);
        LocalTime time = LocalTime.now().plusMinutes(60);
        Cinemahall cinemahall = cinemaHallDao.findAll().get(0);
        Movie movie = movieDao.findAll().get(0);
        Projection projection = new Projection("CZ", 129, date, time, null, cinemahall, movie);
        projectionDao.persist(projection);

        //projection is in a 1 day and 60 minutes
        date = LocalDate.now().plusDays(1);
        time = LocalTime.now().plusMinutes(60);
        cinemahall = cinemaHallDao.findAll().get(0);
        movie = movieDao.findAll().get(1);
        projection = new Projection("CZ", 99, date, time, null, cinemahall, movie);
        projectionDao.persist(projection);

        //projection is in a 30 minutes
        date = LocalDate.now();
        time = LocalTime.now().plusMinutes(30);
        cinemahall = cinemaHallDao.findAll().get(0);
        movie = movieDao.findAll().get(1);
        projection = new Projection("CZ", 149, date, time, null, cinemahall, movie);
        projectionDao.persist(projection);

    }

    private void createMovies() {
        Movie movie = new Movie("Inception", "Dominick \"Dom\" Cobb and Arthur are \"extractors\": they perform corporate espionage using experimental military technology to infiltrate their targets’ subconscious and extract information through a shared dream world. Their latest target, Saito, reveals he arranged their mission to test Cobb for a seemingly impossible job: implanting an idea in a person's subconscious, or \"inception\". Saito wants Cobb to convince Robert, the son of Saito’s ailing competitor Maurice Fischer, to dissolve his father's company. In return, Saito promises to clear Cobb's criminal status, which prevents him from returning home to his children.", (short)120, (short)8, null, "waiting for image");
        movieDao.persist(movie);

        movie = new Movie("Interstellar", "Interstellar is a 2014 epic science fiction film directed, co-written and co-produced by Christopher Nolan. ... Set in a dystopian future where humanity is struggling to survive, the film follows a group of astronauts who travel through a wormhole near Saturn in search of a new home for humanity.", (short)100, (short)12, null, "waiting for image");
        movieDao.persist(movie);

        movie = new Movie("Tenet", "Armed with only one word -- Tenet -- and fighting for the survival of the entire world, the Protagonist journeys through a twilight world of international espionage on a mission that will unfold in something beyond real time.", (short)300, (short)18, null, "waiting for image");
        movieDao.persist(movie);
    }

    private void createCinemasAndCinemaHalls() {
        //creating first cinema and its cinemahalls
        Cinema cinema = new Cinema("Cinema Smíchov", "Praha 5");
        cinemaDao.persist(cinema);

        Cinemahall cinemahall = new Cinemahall((short)1, (short)50, cinema);
        cinemaHallDao.persist(cinemahall);

        cinemahall = new Cinemahall((short)2, (short)40, cinema);
        cinemaHallDao.persist(cinemahall);

        //creating second cinema and its cinemahalls
        cinema = new Cinema("Cinema Dejvická", "Praha 6");
        cinemaDao.persist(cinema);

        cinemahall = new Cinemahall((short)1, (short)60, cinema);
        cinemaHallDao.persist(cinemahall);

        cinemahall = new Cinemahall((short)2, (short)20, cinema);
        cinemaHallDao.persist(cinemahall);
        cinemahall = new Cinemahall((short)3, (short)20, cinema);
        cinemaHallDao.persist(cinemahall);
        cinemahall = new Cinemahall((short)4, (short)20, cinema);
        cinemaHallDao.persist(cinemahall);
    }

    private void createEmployees() {
        Person person = new Employee("Employee1", "EmployeeSur1", "employee1@gmail.com", "password1",
                Role.EMPLOYEE, null, 13);
        personService.persist(person);
        System.out.println("Test user Employee1 persist.");

        person = new Employee("Employee2", "EmployeeSur2", "employee2@gmail.com", "password1",
                Role.EMPLOYEE, null, 14);
        personService.persist(person);
        System.out.println("Test user Employee2 persist.");
    }

    private void createAdmin() {
        Person person = new Employee("Admin1", "AdminSur1", "admin@gmail.com", "admin123",
                Role.ADMIN, null, 12);
        personService.persist(person);
        System.out.println("Test user Admin1 persist.");
    }

    private void createPersons() {
        Person person = new Person("User1", "UserSur1", "user1@gmail.com", "password1",
                Role.PERSON, null);

        personService.persist(person);
        System.out.println("Test user User1 persist.");

        person = new Person("User2", "UserSur2", "user2@gmail.com", "heslOO123",
                Role.PERSON, null);
        personService.persist(person);
        System.out.println("Test user User2 persist.");

        person = new Person("User3", "UserSur3", "user3@gmail.com", "admin",
                Role.PERSON, null);
        personService.persist(person);
        System.out.println("Test user User3 persist.");
    }
}
