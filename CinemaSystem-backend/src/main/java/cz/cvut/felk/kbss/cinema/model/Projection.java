/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.felk.kbss.cinema.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "projection")
@XmlRootElement
@NamedQueries({
      @NamedQuery(name = "Projection.findAll", query = "SELECT p FROM Projection p")
    , @NamedQuery(name = "Projection.findByDabbing", query = "SELECT p FROM Projection p WHERE p.dabbing = :dabbing")
    , @NamedQuery(name = "Projection.findByCinema", query = "SELECT p FROM Projection p JOIN p.cinemahall c WHERE c.cinema = :cinema")
    , @NamedQuery(name = "Projection.findByDate", query = "SELECT p FROM Projection p WHERE p.date = :date")
    , @NamedQuery(name = "Projection.findByMovie", query = "SELECT p from Projection p WHERE p.movie = :movie")
    , @NamedQuery(name = "Projection.getProjectionsByCinemahall",query = "SELECT p FROM Projection p WHERE :cinemahall = p.cinemahall")
    , @NamedQuery(name = "Projection.getProjectionsByDate",query = "SELECT p FROM Projection p WHERE :date = p.date")
    , @NamedQuery(name = "Projection.getCurrentProjections", query = "SELECT p FROM Projection p WHERE :date <= p.date")
})
public class Projection implements Serializable {
    @GeneratedValue
    @Id
    @Column(name = "id")
    private Integer id;

    @Size(max = 30)
    @Column(name = "dabbing")
    private String dabbing;

    @NotNull
    @Column(name = "price")
    private int price;

    @Column(name = "date")
    private LocalDate date;

    @Column(name = "time")
    private LocalTime time;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "projection")
    private List<Reservation> reservationList;

    @JoinColumn(name = "cinemahall", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Cinemahall cinemahall;

    @JoinColumn(name = "movie", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Movie movie;

    public Projection(){}

    public Projection(@Size(max = 30) String dabbing, @NotNull int price, LocalDate date, LocalTime time,
                      List<Reservation> reservationList, Cinemahall cinemahall, Movie movie) {
        this.dabbing = dabbing;
        this.price = price;
        this.date = date;
        this.time = time;
        this.reservationList = reservationList;
        this.cinemahall = cinemahall;
        this.movie = movie;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDabbing() {
        return dabbing;
    }

    public void setDabbing(String dabbing) {
        this.dabbing = dabbing;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public LocalTime getTime() {
        return time;
    }

    public void setTime(LocalTime time) {
        this.time = time;
    }

    @XmlTransient
    public List<Reservation> getReservationList() {
        return reservationList;
    }

    public void setReservationList(List<Reservation> reservationList) {
        this.reservationList = reservationList;
    }

    public Cinemahall getCinemahall() {
        return cinemahall;
    }

    public void setCinemahall(Cinemahall cinemahall) {
        this.cinemahall = cinemahall;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    @Override
    public String toString() {
        return "Projections{" +
                "movie = '" + movie.getName() +
                "', date = " + getDate() +
                "', time = " + getTime() +
                "', cinema = " + getCinemahall().getCinema() +
                "}";
    }
}
