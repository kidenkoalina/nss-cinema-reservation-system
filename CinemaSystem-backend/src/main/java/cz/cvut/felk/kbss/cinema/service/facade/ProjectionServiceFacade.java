package cz.cvut.felk.kbss.cinema.service.facade;

import cz.cvut.felk.kbss.cinema.model.Projection;
import cz.cvut.felk.kbss.cinema.model.Reservation;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.time.LocalDate;
import java.util.List;

public interface ProjectionServiceFacade {

    /**
     * returns all projections.
     * @return projection
     */
    List<Projection> getProjections();

    /**
     * returns all projections in defined time
     * @param date when projection occurs
     * @return projection
     */
    List<Projection> getProjections(LocalDate date);

    /**
     * returns projection by it's id
     * @param id of the projection
     * @return projection
     */
    Projection getProjection(Integer id);

    /**
     * returns reservations by id of it's projection
     * @param id of the projection
     * @return reservation
     */
    List<Reservation> getReservations(Integer id);

    /**
     * returns all projections by cinema
     * @param cinemaName name of the cinema
     * @return projection
     */
    List<Projection> getProjectionsByCinema(String cinemaName);

    /**
     * returns all projections by movie
     * @param movieName name of the movie
     * @return projection
     */
    List<Projection> getProjectionsByMovie(String movieName);

    /**
     *  returns statistics of given projection
     * @param id of the projection
     * @return string of statistics
     */
    String getStatisticsByProjection(@PathVariable Integer id);

    /**
     *  adds (creates) a new projection to projection list
     * @param projection projection to be created
     * @return id of created projection
     */
    int addNewProjection(Projection projection);

    /**
     *  updates existed projection
     * @param id of the projection to be updated
     * @param projection new values in projection to get updated
     */
    void updateProjection(Integer id, Projection projection);

    /**
     * cancels unconfirmed reservation by it's projection's id
     * @param id of the projection
     */
    void cancelUnconfirmedReservationsByProjection(Integer id);

}
