package cz.cvut.felk.kbss.cinema.service.services;

import cz.cvut.felk.kbss.cinema.dao.CinemaDao;
import cz.cvut.felk.kbss.cinema.model.Cinema;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;

@Service
public class CinemaService {

    private final CinemaDao cinemaDao;

    @Autowired
    public CinemaService(CinemaDao cinemaDao){this.cinemaDao = cinemaDao;}

    @Transactional(readOnly = true)
    public List<Cinema> findAll(){
        return cinemaDao.findAll();
    }

    @Transactional
    public void persist(Cinema cinema) {
        Objects.requireNonNull(cinema);
        cinemaDao.persist(cinema);
    }

    @Transactional(readOnly = true)
    public Cinema find(Integer id) {
        return cinemaDao.find(id);
    }

    @Transactional
    public Cinema findByName(String name) {
        return cinemaDao.findByName(name);
    }

    @Transactional
    public void update(Cinema cinema) {
        cinemaDao.update(cinema);
    }

    @Transactional
    public void remove(Cinema cinema){
        Objects.requireNonNull(cinema);
        cinemaDao.remove(cinema);
    }
}
