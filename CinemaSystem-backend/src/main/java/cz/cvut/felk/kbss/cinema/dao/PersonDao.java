package cz.cvut.felk.kbss.cinema.dao;

import cz.cvut.felk.kbss.cinema.model.Person;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class  PersonDao extends BaseDao<Person> {
    public PersonDao(){
        super(Person.class);
    }

    public List<Person> findAll() {
        try {
            List<Person> persons = new ArrayList<>();
            List<Person> persons1 = em.createNamedQuery("Person.findAll", Person.class).getResultList();
            for (Person p : persons1){
                if(p.isPerson()) persons.add(p);
            }
            return persons;
        } catch (NoResultException e) {
            return null;
        }
    }

    public Person findByEmail(String email) {
        try {
            return em.createNamedQuery("Person.findByEmail", Person.class).setParameter("email", email)
                    .getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }
}
