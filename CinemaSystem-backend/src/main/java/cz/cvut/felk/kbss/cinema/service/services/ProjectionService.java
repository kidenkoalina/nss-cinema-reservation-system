package cz.cvut.felk.kbss.cinema.service.services;

import cz.cvut.felk.kbss.cinema.dao.ProjectionDao;
import cz.cvut.felk.kbss.cinema.model.Cinema;
import cz.cvut.felk.kbss.cinema.model.Movie;
import cz.cvut.felk.kbss.cinema.model.Projection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

@Service
public class ProjectionService {

    private final ProjectionDao projectionDao;

    @Autowired
    public ProjectionService(ProjectionDao projectionDao) {
        this.projectionDao = projectionDao;
    }

    @Transactional(readOnly = true)
    public List<Projection> findAll(){
        return projectionDao.findAll();
    }

    @Transactional(readOnly = true)
    public Projection find(Integer id) {
        return projectionDao.find(id);
    }

    @Transactional(readOnly = true)
    public List<Projection> findByMovie(Movie movie){
        return projectionDao.findByMovie(movie);
    }

    @Transactional(readOnly = true)
    public List<Projection> findByDate(LocalDate date) {
        return projectionDao.getProjectionsByDate(date);
    }

    @Transactional
    public List<Projection> findByCinema(Cinema cinema) {
        return projectionDao.findByCinema(cinema);
    }

    @Transactional
    public List<Projection> getCurrentProjections(LocalDate date){
        return projectionDao.getCurrentProjections(date);
    }



    @Transactional
    public void update(Projection projection) {
        projectionDao.update(projection);
    }

    @Transactional
    public void persist(Projection projection) {
        Objects.requireNonNull(projection);
        projectionDao.persist(projection);
    }
}
