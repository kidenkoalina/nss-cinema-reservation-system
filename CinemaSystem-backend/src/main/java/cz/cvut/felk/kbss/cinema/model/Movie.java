/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.felk.kbss.cinema.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.List;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "movie")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Movie.findAll", query = "SELECT m FROM Movie m")
    , @NamedQuery(name = "Movie.findByName", query = "SELECT m FROM Movie m WHERE m.name = :name")
    , @NamedQuery(name = "Movie.findByAgeRate", query = "SELECT m FROM Movie m WHERE m.ageRate = :ageRate")})
public class Movie implements Serializable {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Integer id;

    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "name")
    private String name;

    @Size(max = 21474836)
    @Column(name = "description",columnDefinition = "TEXT")
    private String description;

    @NotNull
    @Column(name = "length")
    private short length;

    @Column(name = "age_rate")
    private Short ageRate;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "movie", orphanRemoval = true)
    private List<Projection> projectionList;

    @Column(name = "image")
    private String image;

    public Movie(){}

    public Movie(@NotNull @Size(min = 1, max = 100) String name, @Size(max = 21474836) String description,
                 @NotNull short length, Short ageRate, List<Projection> projectionList, String image) {
        this.name = name;
        this.description = description;
        this.length = length;
        this.ageRate = ageRate;
        this.projectionList = projectionList;
        this.image = image;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public short getLength() {
        return length;
    }

    public void setLength(short length) {
        this.length = length;
    }

    public Short getAgeRate() {
        return ageRate;
    }

    public void setAgeRate(Short ageRate) {
        this.ageRate = ageRate;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @XmlTransient
    public List<Projection> getProjectionList() {
        return projectionList;
    }

    public void setProjectionList(List<Projection> projectionList) {
        this.projectionList = projectionList;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "name = '" + name + "', length = " + length +
                "}";
    }
}
