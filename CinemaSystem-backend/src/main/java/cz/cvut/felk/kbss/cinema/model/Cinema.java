/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.felk.kbss.cinema.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.List;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "cinema")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Cinema.findAll", query = "SELECT c FROM Cinema c ORDER BY c.id")
    , @NamedQuery(name = "Cinema.findByName", query = "SELECT c FROM Cinema c WHERE c.name = :name")
    , @NamedQuery(name = "Cinema.findByTown", query = "SELECT c FROM Cinema c WHERE c.town = :town")})
public class Cinema implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @NotNull
    @Column(name = "name")
    private String name;

    @NotNull
    @Column(name = "town")
    private String town;

    @JsonIgnore
    @OneToMany(mappedBy = "cinema", cascade = CascadeType.ALL)
    @OrderBy("number") //preusporada seznam cinemahallu podle cisel a ne podle id.
    private List<Cinemahall> cinemahallList;

    public Cinema(){}

    public Cinema(@NotNull String name, @NotNull String town) {
        this.name = name;
        this.town = town;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    @XmlTransient
    public List<Cinemahall> getCinemahallList() {
        return cinemahallList;
    }

    public void setCinemahallList(List<Cinemahall> cinemahallList) {
        this.cinemahallList = cinemahallList;
    }

    @Override
    public String toString() {
        return "Cinema{" +
                "name = '" + name + "', town = " + town +
                "}";
    }
}
