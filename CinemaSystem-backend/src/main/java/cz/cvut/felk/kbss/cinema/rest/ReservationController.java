package cz.cvut.felk.kbss.cinema.rest;

import cz.cvut.felk.kbss.cinema.exception.NotFoundException;
import cz.cvut.felk.kbss.cinema.exception.ValidationException;
import cz.cvut.felk.kbss.cinema.model.Reservation;
import cz.cvut.felk.kbss.cinema.rest.util.RestUtils;
import cz.cvut.felk.kbss.cinema.service.services.ReservationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/reservations")
public class ReservationController {

    private static final Logger LOG = LoggerFactory.getLogger(ReservationController.class);

    private final ReservationService reservationService;

    @Autowired
    public ReservationController(ReservationService reservationService) {
        this.reservationService = reservationService;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_EMPLOYEE')")
    public List<Reservation> getReservations() {
        return reservationService.findAll();
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_PERSON', 'ROLE_EMPLOYEE')")
    public ResponseEntity<String> addNewReservation(@RequestBody Reservation r){
        try{
            reservationService.addNewReservation(r);
            final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/{id}", r.getId());
            return new ResponseEntity<>(headers, HttpStatus.CREATED);
        }catch (Exception e){
            final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/{id}", r.getId());
            return new ResponseEntity<>(e.getMessage(),headers, HttpStatus.CONFLICT);
        }


    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_EMPLOYEE')")
    public Reservation getReservation(@PathVariable Integer id) {
        final Reservation reservation = reservationService.find(id);
        if (reservation == null) {
            throw NotFoundException.create("Reservation", id);
        }
        return reservation;
    }

    @GetMapping(value = "/{id}/confirm")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_EMPLOYEE')")
    public ResponseEntity<String> confirmReservation(@PathVariable Integer id){
        final Reservation reservation = reservationService.find(id);
        try{
            reservationService.confirmReservation(reservation);
            LOG.debug("Confirmed reservation {}.", reservation);
            final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/{id}", reservation.getId());
            return new ResponseEntity<>(headers, HttpStatus.NO_CONTENT);
        }catch (Exception e){
            final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/{id}", reservation.getId());
            return new ResponseEntity<>(e.getMessage(),headers, HttpStatus.CONFLICT);
        }
    }

    @DeleteMapping(value = "/{id}/cancel")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_PERSON', 'ROLE_EMPLOYEE')")
    public ResponseEntity<String> cancelReservation(@PathVariable Integer id){
        final Reservation reservation = reservationService.find(id);
        try{
            reservationService.cancelReservation(reservation);
            LOG.debug("Cancelled reservation {}.", reservation);
            final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/{id}", reservation.getId());
            return new ResponseEntity<>(headers, HttpStatus.NO_CONTENT);
        }catch (Exception e){
            final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/{id}", reservation.getId());
            return new ResponseEntity<>(e.getMessage(),headers, HttpStatus.CONFLICT);
        }
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateReservation(@PathVariable Integer id, @RequestBody Reservation reservation) {
        final Reservation original = getReservation(id);
        if (!original.getId().equals(reservation.getId())) {
            throw new ValidationException("Reservation identifier in the data does not match the one in the request URL.");
        }
        reservationService.update(reservation);
        LOG.debug("Updated reservation {}.", reservation);
    }
}
