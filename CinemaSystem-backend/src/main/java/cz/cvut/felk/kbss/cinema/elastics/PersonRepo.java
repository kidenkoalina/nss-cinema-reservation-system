package cz.cvut.felk.kbss.cinema.elastics;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonRepo extends ElasticsearchRepository<PersonElastic, Integer> {
}
