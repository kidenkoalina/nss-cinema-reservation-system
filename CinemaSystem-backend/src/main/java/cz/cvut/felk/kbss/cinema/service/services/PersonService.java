package cz.cvut.felk.kbss.cinema.service.services;

import cz.cvut.felk.kbss.cinema.dao.PersonDao;
import cz.cvut.felk.kbss.cinema.model.Movie;
import cz.cvut.felk.kbss.cinema.model.Person;
import cz.cvut.felk.kbss.cinema.model.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;

@Service
public class PersonService {

    private final PersonDao personDao;

    //security
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public PersonService(PersonDao personDao, PasswordEncoder passwordEncoder) {
        this.personDao = personDao;
        this.passwordEncoder = passwordEncoder;
    }

    @Transactional(readOnly = true)
    public List<Person> findAll(){
        return personDao.findAll();
    }

    @Transactional
    public void persist(Person person) {
        Objects.requireNonNull(person);
        person.encodePassword(passwordEncoder);
        if (person.getRole() == null) {
            person.setRole(Role.PERSON);
        }
        personDao.persist(person);
    }

    @Transactional
    public void update(Person person) {
        personDao.update(person);
    }

    @Transactional(readOnly = true)
    public Person find(Integer id) {
        return personDao.find(id);
    }

    @Transactional(readOnly = true)
    public boolean exists(String email) {
        return personDao.findByEmail(email) != null;
    }
}
