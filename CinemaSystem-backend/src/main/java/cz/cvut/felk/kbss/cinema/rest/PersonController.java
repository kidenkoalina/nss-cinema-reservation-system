package cz.cvut.felk.kbss.cinema.rest;

import cz.cvut.felk.kbss.cinema.exception.NotFoundException;
import cz.cvut.felk.kbss.cinema.exception.ValidationException;
import cz.cvut.felk.kbss.cinema.model.Person;
import cz.cvut.felk.kbss.cinema.model.Reservation;
import cz.cvut.felk.kbss.cinema.rest.util.RestUtils;
import cz.cvut.felk.kbss.cinema.security.model.AuthenticationToken;
import cz.cvut.felk.kbss.cinema.service.services.PersonService;
import cz.cvut.felk.kbss.cinema.service.services.ReservationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.Principal;
import java.util.List;


@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/user")
public class PersonController {

    private static final Logger LOG = LoggerFactory.getLogger(PersonController.class);

    private final PersonService personService;
    private final ReservationService reservationService;


    @Autowired
    public PersonController(PersonService personService, ReservationService reservationService) {
        this.personService = personService;
        this.reservationService = reservationService;
    }

    //READ ALL PERSONS
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Person> getPersons(){
        return personService.findAll();
    }

    //READ 1 PERSON
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Person getPerson(@PathVariable Integer id) {
        final Person person = personService.find(id);
        if (person == null) {
            throw NotFoundException.create("Person", id);
        }
        return person;
    }

    //CREATE PERSON
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> registerPerson(@RequestBody Person p){
        personService.persist(p);
        LOG.debug("Person {} successfully registered.", p);
        final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/current");
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    //TODO ALARM!ALARM! duplicit method (second one is in EmployeeController)
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_PERSON', 'ROLE_EMPLOYEE')")
    @GetMapping(value = "/current", produces = MediaType.APPLICATION_JSON_VALUE)
    public Person getCurrent(Principal principal) {
        final AuthenticationToken auth = (AuthenticationToken) principal;
        return auth.getPrincipal().getUser();
    }

    //UPDATE PERSON
    @PreAuthorize("hasRole('ROLE_PERSON')")
    @PatchMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updatePerson(@PathVariable Integer id, @RequestBody Person person) {
        final Person original = getPerson(id);
        if (!original.getId().equals(person.getId())) {
            throw new ValidationException("Person identifier in the data does not match the one in the request URL.");
        }
        personService.update(person);
        LOG.debug("Updated person {}.", person);
    }

    @GetMapping(value = "/{id}/reservations", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Reservation> getReservationsByPerson(@PathVariable Integer id) {
        final Person person = getPerson(id);
        return reservationService.findByPerson(person);
    }
    @RequestMapping(value="/logout", method = RequestMethod.GET)
    public String logoutPage (HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null){
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
         return "redirect:/login";
    }
}

