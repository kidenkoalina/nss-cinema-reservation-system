package cz.cvut.felk.kbss.cinema.model;

public enum StateOfReservation {
    CREATED,
    CANCELLED,
    CONFIRMED;
}
