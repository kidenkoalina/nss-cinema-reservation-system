package cz.cvut.felk.kbss.cinema.rest;

import cz.cvut.felk.kbss.cinema.exception.NotFoundException;
import cz.cvut.felk.kbss.cinema.exception.ValidationException;
import cz.cvut.felk.kbss.cinema.model.Cinema;
import cz.cvut.felk.kbss.cinema.model.Cinemahall;
import cz.cvut.felk.kbss.cinema.rest.util.RestUtils;
import cz.cvut.felk.kbss.cinema.service.services.CinemaHallService;
import cz.cvut.felk.kbss.cinema.service.services.CinemaService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/cinemas")
public class CinemaController {

    private static final Logger LOG = LoggerFactory.getLogger(CinemaController.class);

    private final CinemaService cinemaService;
    private final CinemaHallService cinemaHallService;
    @Autowired
    public CinemaController(CinemaService cinemaService, CinemaHallService cinemaHallService){
        this.cinemaService = cinemaService;
        this.cinemaHallService = cinemaHallService;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Cinema> getCinemas(){
        return cinemaService.findAll();
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> addNewCinema(@RequestBody Cinema c){
        cinemaService.persist(c);
        //TODO:pridat LOGGER
        final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/{id}", c.getId());
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Cinema getCinema(@PathVariable Integer id) {
        final Cinema cinema = cinemaService.find(id);
        if (cinema == null) {
            throw NotFoundException.create("Cinema", id);
        }
        return cinema;
    }
    @GetMapping(value = "/{id}/halls", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Cinemahall> getCinemaHalls(@PathVariable Integer id) {
        final Cinema cinema = cinemaService.find(id);
        if (cinema == null) {
            throw NotFoundException.create("Cinema", id);
        }
        return cinemaHallService.findByCinema(cinema);
    }
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateCinema(@PathVariable Integer id, @RequestBody Cinema cinema) {
        final Cinema original = getCinema(id);
        if (!original.getId().equals(cinema.getId())) {
            throw new ValidationException("Cinema identifier in the data does not match the one in the request URL.");
        }
        cinemaService.update(cinema);
        LOG.debug("Updated cinema {}.", cinema);
    }
}
