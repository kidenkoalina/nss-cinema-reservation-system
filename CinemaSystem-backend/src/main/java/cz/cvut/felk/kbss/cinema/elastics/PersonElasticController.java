package cz.cvut.felk.kbss.cinema.elastics;

import cz.cvut.felk.kbss.cinema.elastics.PersonElastic;
import cz.cvut.felk.kbss.cinema.elastics.PersonRepo;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.*;


@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/elastic/persons")
public class PersonElasticController {

    private static final Logger LOG = LoggerFactory.getLogger(PersonElasticController.class);

    @Autowired
    private PersonRepo personRepository;


    /**
     * Creates a new Person using ElasticSearch
     * @param person defined Person body structure
     * @return CREATED if the person was successfully created.
     */
    @PostMapping("/create")
    public ResponseEntity<?> create(@RequestBody PersonElastic person)  {
        PersonElastic p = personRepository.save(person);
        return new ResponseEntity<>(p, HttpStatus.CREATED);
    }

    /**
     * Gets person by it's ID.
     * @param id of Person
     * @return found person
     */
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping("/{id}")
    public Optional<PersonElastic> getPerson(@PathVariable final Integer id) {
        return personRepository.findById(id);
    }


    /**
     * Returns all created persons
     * @return persons
     */
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<PersonElastic> getAllPersons() {
        List<PersonElastic> persons = new ArrayList<>();
        personRepository.findAll().forEach(persons::add);
        return persons;
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<?> update(@RequestBody PersonElastic person, @PathVariable final Integer id) {
        PersonElastic p = personRepository.save(person);
        return new ResponseEntity<>(p, HttpStatus.OK);
    }


    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> delete(@PathVariable  Integer id) {
        personRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }




}
