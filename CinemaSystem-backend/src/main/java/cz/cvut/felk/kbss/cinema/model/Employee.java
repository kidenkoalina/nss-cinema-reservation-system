/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.felk.kbss.cinema.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@NamedQueries({
    @NamedQuery(name = "Employee.findAll", query = "SELECT e FROM Employee e"),
    @NamedQuery(name = "Employee.findByEmail", query = "SELECT e FROM Employee e WHERE e.email = :email"),
    @NamedQuery(name = "Employee.findByEmployeeNumber", query = "SELECT e FROM Employee e WHERE e.employeeNumber = :employeeNumber")})
@DiscriminatorValue("e")
public class Employee extends Person implements Serializable {

    @NotNull
    @Column(name = "employee_number")
    private Integer employeeNumber;

    public Employee(){}

    public Employee(@NotNull @Size(min = 1, max = 50) String name, @NotNull @Size(min = 1, max = 50) String surname,
                    @NotNull @Size(min = 1, max = 100) String email, @NotNull @Size(min = 1, max = 100) String password,
                    Role role, List<Reservation> reservationList, @NotNull Integer employeeNumber) {
        super(name, surname, email, password, role, reservationList);
        this.employeeNumber = employeeNumber;
    }

    public Integer getEmployeeNumber() {
        return employeeNumber;
    }

    public void setEmployeeNumber(Integer employeeNumber) {
        this.employeeNumber = employeeNumber;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "employeeNumber = '" + employeeNumber +
                "'}";
    }
}
