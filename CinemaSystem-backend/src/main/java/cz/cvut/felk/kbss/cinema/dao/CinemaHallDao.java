package cz.cvut.felk.kbss.cinema.dao;

import cz.cvut.felk.kbss.cinema.model.Cinema;
import cz.cvut.felk.kbss.cinema.model.Cinemahall;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.List;
import java.util.Objects;

@Repository
public class CinemaHallDao extends BaseDao<Cinemahall>{
    public CinemaHallDao(){
        super(Cinemahall.class);
    }
    public List<Cinemahall> findByName(Cinema cinemaName) {
        try {
            return em.createNamedQuery("Cinemahall.findByCinema", Cinemahall.class).setParameter("cinema", cinemaName)
                    .getResultList();
        } catch (NoResultException e) {
            return null;
        }
    }
}
