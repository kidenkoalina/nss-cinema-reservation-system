package cz.cvut.felk.kbss.cinema.dao;

import cz.cvut.felk.kbss.cinema.model.Movie;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import java.util.List;
import java.util.Objects;

@Repository
public class MovieDao extends BaseDao<Movie>{
    public MovieDao(){
        super(Movie.class);
    }

    public Movie findByName(String name){
        Objects.requireNonNull(name);
        return em.createNamedQuery("Movie.findByName", Movie.class).setParameter("name", name)
                .getSingleResult();
    }

    //napriklad pro budouci funkci kdyz rodic bude chtit vyhledat vsechny filmy pro deti
    public Movie findByAgeRate(Integer ageRate){
        Objects.requireNonNull(ageRate);
        return em.createNamedQuery("Movie.findByAgeRate", Movie.class).setParameter("ageRate", ageRate)
                .getSingleResult();
    }

    public List<Movie> findAll() {
        try {
            return em.createNamedQuery("Movie.findAll", Movie.class)
                    .getResultList();
        } catch (NoResultException e) {
            return null;
        }
    }
}
